﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CapaEntidad;
using CapaNegocio;
namespace WindowsFormsApp1
{
    public partial class FrmPrincipal : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);


        public FrmPrincipal()
        {
            InitializeComponent();
            
        }
        int codigoEntidad;
        public FrmPrincipal(int _codigoEntidad)
        {
            InitializeComponent();
            codigoEntidad = _codigoEntidad;

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void maximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            maximizar.Visible = false;
            restaurar.Visible = true;

        }

        private void minimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void restaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            restaurar.Visible = false;
            maximizar.Visible = true;
        }

        private void btnRecojo_Click(object sender, EventArgs e)
        {
            subPanelRecojo.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            subPanelRecojo.Visible = false;
        }

        private void AbrirFormHijo(object formHijo)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = formHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new Perfil(codigoEntidad));
        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            DataTable tabla = new N_Entidad().ListUsers();
            AbrirFormHijo(new FrmUsuarios(tabla));
        }

        private void btnCerrrarSesion_Click(object sender, EventArgs e)
        {
            Close();
            FrmLogin Login = new FrmLogin();
            Login.Show();
        }

        private void panelVertical_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
