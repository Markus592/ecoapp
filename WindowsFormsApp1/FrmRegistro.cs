﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

using CapaEntidad;
using CapaNegocio;


namespace WindowsFormsApp1
{
    public partial class FrmRegistro : Form
    {
        public FrmRegistro()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void altoTextBox1_Click(object sender, EventArgs e)
        {

        }


        N_Ubigeo ubigeo = new N_Ubigeo();
        N_TipoDocumento tipoDocumento = new N_TipoDocumento();
        E_Entidad entidad = new E_Entidad();
        N_Entidad n_Entidad = new N_Entidad();



        private void FrmRegistro_Load(object sender, EventArgs e)
        {

            cmbProvincia.Enabled = false;
            cmbDistrito.Enabled = false;
            //para listar data de departamentos
            cmbDepartamento.DataSource = ubigeo.MostrarDepartamentos();
            cmbDepartamento.ValueMember = "codigo";
            cmbDepartamento.DisplayMember = "Nombre";
            //lista para tipo_doc
            cmbTipoDocumento.DataSource = tipoDocumento.TipoDocumento();
            cmbTipoDocumento.ValueMember = "codigo";
            cmbTipoDocumento.DisplayMember ="Nombre";

            //para listar data de tipos de documentos
            // cmbTipoDocumento.DataSource = tipoDocumento.MostrarTipoDocumento();
            cmbTipoDocumento.ValueMember = "Codigo";
            cmbTipoDocumento.DisplayMember = "Nombre";
            txtContrasenia.Text = "";
            txtContrasenia.PasswordChar = true;

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            


        }
      

        private void minimizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void maximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            maximizar.Visible = false;
            restaurar.Visible = true;

        }

        private void cerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void restaurar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            restaurar.Visible = false;
            maximizar.Visible = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }




        
        private void btnRegistrar_Click(object sender, EventArgs e)
        {


                          if (n_Entidad.RegistrarColaborador(entidad,""))
                          {
                              MessageBox.Show("Usuario creado satisfactoriamente");
                              FrmPrincipal main = new FrmPrincipal(entidad.Codigo);
                              main.Show();
                              Hide();
                          }



            try
            {
                entidad.Nombres = txtNombre.Text.Trim();
                entidad.ApellidoPaterno = txtApellidoPaterno.Text.Trim();
                entidad.ApellidoMaterno = txtApellidoMaterno.Text.Trim();
               // entidad.Direccion = txtDireccion.Text.Trim();
                entidad.IdUbigeo = Convert.ToInt16(cmbDistrito.SelectedValue);
                entidad.IdDocumento = Convert.ToInt16(cmbTipoDocumento.SelectedValue);
                entidad.Documento = txtDocumento.Text.Trim();
                entidad.NumeroTelefonico = txtCelular.Text.Trim();
                entidad.Usuario = txtCorreo.Text.Trim();
                
                entidad.Password = txtContrasenia.Text.Trim();
               // entidad.FechaNacimiento = dtpFechaNacimiento.Value;
             
                n_Entidad.RegistrarCliente(entidad,txtVerificarContra.Text);


                MessageBox.Show("Cliente insertado correctamente");
                Close();
                //MessageBox.Show(N_Entidad.EspaciosObligatorios(entidad));
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }






















        }



       

        private void txtNombre_Click(object sender, EventArgs e)
        {

            
            
            
        }



        //  enter-lavel nombre

        private void txtNombre_Enter(object sender, EventArgs e)
        {
            if (txtNombre.Text == "Nombre") 
            {
                txtNombre.Text = "";
                txtNombre.ForeColor = Color.SeaGreen;


            }
        }

        

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (txtNombre.Text=="") 
            {
                txtNombre.Text = "Nombre";
                txtNombre.ForeColor = Color.SeaGreen;
            
            }
        }

        //   enter-lavel apellido paterno
        private void txtApellidoPaterno_Enter(object sender, EventArgs e)
        {
            if (txtApellidoPaterno.Text == "Apellido Paterno")
            {
                txtApellidoPaterno.Text = "";
               txtApellidoPaterno.ForeColor = Color.SeaGreen;


            }
        }

        private void txtApellidoPaterno_Leave(object sender, EventArgs e)
        {
            if (txtApellidoPaterno.Text == "")
            {
                txtApellidoPaterno.Text = "Apellido Paterno";
                txtApellidoPaterno.ForeColor = Color.SeaGreen;


            }
        }


        // apellido materno
        private void txtApellidoMaterno_Enter(object sender, EventArgs e)
        {
            if (txtApellidoMaterno.Text == "Apellido Materno")
            {
                txtApellidoMaterno.Text = "";
                txtApellidoMaterno.ForeColor = Color.SeaGreen;


            }
        }

        private void txtApellidoMaterno_Leave(object sender, EventArgs e)
        {
            if (txtApellidoMaterno.Text == "")
            {
                txtApellidoMaterno.Text = "Apellido Materno";
                txtApellidoMaterno.ForeColor = Color.SeaGreen;


            }
        }



        //direccion
        private void txtDireccion_Enter(object sender, EventArgs e)
        {
           
        }

        private void txtDireccion_Leave(object sender, EventArgs e)
        {
           
        }


        // celular
        private void txtCelular_Enter(object sender, EventArgs e)
        {
            if (txtCelular.Text == "Celular")
            {
                txtCelular.Text = "";
                txtCelular.ForeColor = Color.SeaGreen;


            }
        }

        private void txtCelular_Leave(object sender, EventArgs e)
        {
            if (txtCelular.Text == "")
            {
                txtCelular.Text = "Celular";
                txtCelular.ForeColor = Color.SeaGreen;


            }
        }


        // correo
        private void txtCorreo_Enter(object sender, EventArgs e)
        {
            if (txtCorreo.Text == "Correo")
            {       
                txtCorreo.Text = "";
                txtCorreo.ForeColor = Color.SeaGreen;


            }
        }

        private void txtCorreo_Leave(object sender, EventArgs e)
        {
            if (txtCorreo.Text == "")
            {
                txtCorreo.Text = "Correo";
                txtCorreo.ForeColor = Color.SeaGreen;


            }
        }

        // contraseña
        private void txtContraseña_Leave(object sender, EventArgs e)
        {
            if (txtContrasenia.Text == "")
            {   
                txtContrasenia.Text = "Contraseña";
                txtContrasenia.ForeColor = Color.SeaGreen;


            }
        }

        private void txtContraseña_Enter(object sender, EventArgs e)
        {
            
                if (txtContrasenia.Text == "Contraseña")
            {
                txtContrasenia.Text = "";
                txtContrasenia.ForeColor = Color.SeaGreen;


            }
        }


        // confirmar contraseña
        private void txtConfirmarContraseña_Enter(object sender, EventArgs e)
        {

            if (txtVerificarContra.Text == "Confirmar contraseña")
            {
                txtVerificarContra.Text = "";
                txtVerificarContra.ForeColor = Color.SeaGreen;

                txtContrasenia.UseSystemPasswordChar = false;
            }
        }

        private void txtConfirmarContraseña_Leave(object sender, EventArgs e)
        {
            if (txtVerificarContra.Text == "")
            {
                txtVerificarContra.Text = "Confirmar contraseña";
                txtVerificarContra.ForeColor = Color.SeaGreen;

               
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            FrmLogin frm = new FrmLogin();

            frm.Show();
            this.Hide();
        }

        private void cmbDepartamento_DropDownClosed(object sender, EventArgs e)
        {
            cmbProvincia.Enabled = true;
            cmbProvincia.DataSource = ubigeo.MostrarProvincias(Convert.ToInt16(cmbDepartamento.SelectedValue));
            cmbProvincia.ValueMember = "codigo";
            cmbProvincia.DisplayMember = "Nombre";
        }

        private void cmbProvincia_DropDownClosed(object sender, EventArgs e)
        {
            cmbDistrito.Enabled = true;
            cmbDistrito.DataSource = ubigeo.MostrarDistritos(Convert.ToInt16(cmbProvincia.SelectedValue));
            cmbDistrito.ValueMember = "codigo";
            cmbDistrito.DisplayMember = "Nombre";
        }

        private void cmbDistrito_DropDownClosed(object sender, EventArgs e)
        {
           
        }

        private void cmbTipoDocumento_DropDownClosed(object sender, EventArgs e)
        {
            cmbTipoDocumento.Enabled = true;
             cmbTipoDocumento.DataSource = ubigeo.MostrarDistritos(Convert.ToInt16(cmbProvincia.SelectedValue));
             cmbTipoDocumento.ValueMember = "codigo";
            cmbTipoDocumento.DisplayMember = "Nombre";
        }
    }
}
