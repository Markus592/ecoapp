﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using CapaNegocio;
using CapaEntidad;

namespace WindowsFormsApp1
{
    public partial class FrmRegistroColaborador : Form
    {
        public FrmRegistroColaborador()
        {
            InitializeComponent();
        }
        public FrmRegistroColaborador(E_Entidad entidad)
        {
            InitializeComponent();
            txtNombre.Text = entidad.Nombres;
            txtApellidoPaterno.Text = entidad.ApellidoPaterno;
            txtApellidoMaterno.Text = entidad.ApellidoMaterno;
            txtDomicilio.Text = entidad.Direccion;
            cmbDistrito.SelectedValue = entidad.IdUbigeo;
            cmbTipoDocumento.SelectedValue = entidad.IdDocumento;
            txtDocumento.Text = entidad.Documento;
            txtTelefono.Text = entidad.NumeroTelefonico;
            txtUsuario.Text = entidad.Usuario;
            txtPassword.Text = entidad.Password;
            txtVerificarContra.Text = entidad.Password;
            
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);


        N_Ubigeo ubigeo = new N_Ubigeo();
      //  N_TipoDocumento tipoDocumento = new N_TipoDocumento();
        E_Entidad entidad = new E_Entidad();
        N_Entidad N_Entidad = new N_Entidad();

        private void FrmRegistro_Load(object sender, EventArgs e)
        {
            cmbProvincia.Enabled = false;
            cmbDistrito.Enabled = false;
            //para listar data de departamentos
            cmbDepartamento.DataSource= ubigeo.MostrarDepartamentos();
            cmbDepartamento.ValueMember = "codigo";
            cmbDepartamento.DisplayMember = "Nombre";
            //para listar data de tipos de documentos
           // cmbTipoDocumento.DataSource = tipoDocumento.MostrarTipoDocumento();
            cmbTipoDocumento.ValueMember = "Codigo";
            cmbTipoDocumento.DisplayMember = "Nombre";
            txtPassword.Text = "";
            txtPassword.PasswordChar =true;


        }



        private void minimizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void maximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            maximizar.Visible = false;
            restaurar.Visible = true;

        }

        private void cerrar_Click_1(object sender, EventArgs e)
        {
            this.Close();
         }

        private void restaurar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            restaurar.Visible = false;
            maximizar.Visible = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                entidad.Nombres = txtNombre.Text.Trim();
                entidad.ApellidoPaterno = txtApellidoPaterno.Text.Trim();
                entidad.ApellidoMaterno = txtApellidoMaterno.Text.Trim();
                entidad.Direccion = txtDomicilio.Text.Trim();
                entidad.IdUbigeo = Convert.ToInt16(cmbDistrito.SelectedValue);
                entidad.IdDocumento = Convert.ToInt16(cmbTipoDocumento.SelectedValue);
                entidad.Documento = txtDocumento.Text.Trim();
                entidad.NumeroTelefonico = txtTelefono.Text.Trim();
                entidad.Usuario = txtUsuario.Text.Trim();
                entidad.Password = txtPassword.Text.Trim();
                entidad.FechaNacimiento = dtpFechaNacimiento.Value;
                N_Entidad.RegistrarColaborador(entidad,txtVerificarContra.Text);
                MessageBox.Show("Se inserto de manera correcta  el colaborador");
                Close();
                //MessageBox.Show(N_Entidad.EspaciosObligatorios(entidad));
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void cmbDepartamento_DropDownClosed(object sender, EventArgs e)
        {
            cmbProvincia.Enabled = true;
            cmbProvincia.DataSource = ubigeo.MostrarProvincias(Convert.ToInt16(cmbDepartamento.SelectedValue));
            cmbProvincia.ValueMember = "codigo";
            cmbProvincia.DisplayMember = "Nombre";
        }

        private void cmbProvincia_DropDownClosed(object sender, EventArgs e)
        {
            cmbDistrito.Enabled = true;
            cmbDistrito.DataSource = ubigeo.MostrarDistritos(Convert.ToInt16(cmbProvincia.SelectedValue));
            cmbDistrito.ValueMember = "codigo";
            cmbDistrito.DisplayMember = "Nombre";
        }

        private void lbFechaNacimiento_Click(object sender, EventArgs e)
        {

        }

        private void txtNombre_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void cmbDistrito_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
