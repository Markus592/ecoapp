﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using CapaDatos;
using CapaEntidad;
namespace CapaNegocio
{
    public class N_LoginEntidad
    {
        public static Boolean email_bien_escrito(String email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {                   
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public static int LoginEntidad(E_Login e_Login)
        {
            D_LoginEntidad d_Login = new D_LoginEntidad();
            E_Login _loginData =d_Login.LoginEntidad(e_Login);
            int codigoEntidad = _loginData.Codigo;                        
            if (e_Login.Correo!= _loginData.Correo||e_Login.Password != _loginData.Password)
            {
                throw new Exception("El correo o contraseña que escribiste no es el correcto");
            }
            
            return codigoEntidad;
        }
      
    }
}
