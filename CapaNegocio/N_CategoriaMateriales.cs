﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data;
using CapaDatos;

namespace CapaNegocio
{
    public class N_CategoriaMateriales
    {
        public DataTable ListarMateriales()
        {
            return new D_CategoriaMateriales().ListarMateriales();
        }
    }
}
