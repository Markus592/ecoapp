﻿
namespace WindowsFormsApp1
{
    partial class FrmListarRecojo
    {
        private System.Windows.Forms.DataGridView DgvRecojo;
        private AltoControls.AltoButton btnBuscar;
        private AltoControls.AltoSlidingLabel lbTituloListaRecojo;
        private AltoControls.AltoButton btnActualizar;
        private AltoControls.AltoButton btnEliminar;
        private AltoControls.AltoButton btnAgregar;
        private System.Windows.Forms.DateTimePicker dtpFechaInicio;
        private System.Windows.Forms.DateTimePicker dtpFechaFin;
        private System.Windows.Forms.Label lbFechaInicio;
        private System.Windows.Forms.Label lbFechaFin;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        // private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        // protected override void Dispose(bool disposing)
        // {
        //     if (disposing && (components != null))
        //     {
        //         components.Dispose();
        //     }
        //     base.Dispose(disposing);
        // }
        //
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        // private void InitializeComponent()
        // {
        //     this.labUsuarios = new AltoControls.AltoSlidingLabel();
        //     this.btnAddUser = new AltoControls.AltoButton();
        //     this.txtDocumento = new AltoControls.AltoTextBox();
        //     this.btnSearchUser = new AltoControls.AltoButton();
        //     this.DgvUsers = new System.Windows.Forms.DataGridView();
        //     this.btnUpdateUser = new AltoControls.AltoButton();
        //     this.btnEliminar = new AltoControls.AltoButton();
        //     ((System.ComponentModel.ISupportInitialize)(this.DgvUsers)).BeginInit();
        //     this.SuspendLayout();
        //     // 
        //     // labUsuarios
        //     // 
        //     this.labUsuarios.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        //     this.labUsuarios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
        //     this.labUsuarios.Location = new System.Drawing.Point(26, 11);
        //     this.labUsuarios.Margin = new System.Windows.Forms.Padding(2);
        //     this.labUsuarios.Name = "labUsuarios";
        //     this.labUsuarios.Size = new System.Drawing.Size(132, 60);
        //     this.labUsuarios.Slide = false;
        //     this.labUsuarios.TabIndex = 4;
        //     this.labUsuarios.Text = "USUARIOS";
        //     // 
        //     // btnAddUser
        //     // 
        //     this.btnAddUser.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
        //     this.btnAddUser.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
        //     this.btnAddUser.BackColor = System.Drawing.Color.Transparent;
        //     this.btnAddUser.DialogResult = System.Windows.Forms.DialogResult.OK;
        //     this.btnAddUser.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
        //     this.btnAddUser.ForeColor = System.Drawing.Color.White;
        //     this.btnAddUser.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnAddUser.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnAddUser.Location = new System.Drawing.Point(329, 23);
        //     this.btnAddUser.Name = "btnAddUser";
        //     this.btnAddUser.Radius = 10;
        //     this.btnAddUser.Size = new System.Drawing.Size(140, 30);
        //     this.btnAddUser.Stroke = false;
        //     this.btnAddUser.StrokeColor = System.Drawing.Color.Gray;
        //     this.btnAddUser.TabIndex = 5;
        //     this.btnAddUser.Text = "Agregar Usuario";
        //     this.btnAddUser.Transparency = false;
        //     this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
        //     // 
        //     // txtDocumento
        //     // 
        //     this.txtDocumento.BackColor = System.Drawing.Color.Transparent;
        //     this.txtDocumento.Br = System.Drawing.Color.White;
        //     this.txtDocumento.Font = new System.Drawing.Font("Century Gothic", 11F);
        //     this.txtDocumento.ForeColor = System.Drawing.Color.DimGray;
        //     this.txtDocumento.Location = new System.Drawing.Point(26, 76);
        //     this.txtDocumento.Name = "txtDocumento";
        //     this.txtDocumento.PasswordChar = false;
        //     this.txtDocumento.Size = new System.Drawing.Size(116, 33);
        //     this.txtDocumento.TabIndex = 6;
        //     this.txtDocumento.Text = "Documento";
        //     this.txtDocumento.Click += new System.EventHandler(this.txtDocumento_Click);
        //     // 
        //     // btnSearchUser
        //     // 
        //     this.btnSearchUser.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
        //     this.btnSearchUser.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
        //     this.btnSearchUser.BackColor = System.Drawing.Color.Transparent;
        //     this.btnSearchUser.DialogResult = System.Windows.Forms.DialogResult.OK;
        //     this.btnSearchUser.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
        //     this.btnSearchUser.ForeColor = System.Drawing.Color.White;
        //     this.btnSearchUser.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnSearchUser.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnSearchUser.Location = new System.Drawing.Point(148, 79);
        //     this.btnSearchUser.Name = "btnSearchUser";
        //     this.btnSearchUser.Radius = 10;
        //     this.btnSearchUser.Size = new System.Drawing.Size(66, 30);
        //     this.btnSearchUser.Stroke = false;
        //     this.btnSearchUser.StrokeColor = System.Drawing.Color.Gray;
        //     this.btnSearchUser.TabIndex = 7;
        //     this.btnSearchUser.Text = "Buscar";
        //     this.btnSearchUser.Transparency = false;
        //     // 
        //     // DgvUsers
        //     // 
        //     this.DgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        //     this.DgvUsers.Location = new System.Drawing.Point(26, 139);
        //     this.DgvUsers.Name = "DgvUsers";
        //     this.DgvUsers.Size = new System.Drawing.Size(430, 221);
        //     this.DgvUsers.TabIndex = 8;
        //     // 
        //     // btnUpdateUser
        //     // 
        //     this.btnUpdateUser.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
        //     this.btnUpdateUser.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
        //     this.btnUpdateUser.BackColor = System.Drawing.Color.Transparent;
        //     this.btnUpdateUser.DialogResult = System.Windows.Forms.DialogResult.OK;
        //     this.btnUpdateUser.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
        //     this.btnUpdateUser.ForeColor = System.Drawing.Color.White;
        //     this.btnUpdateUser.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnUpdateUser.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnUpdateUser.Location = new System.Drawing.Point(148, 383);
        //     this.btnUpdateUser.Name = "btnUpdateUser";
        //     this.btnUpdateUser.Radius = 10;
        //     this.btnUpdateUser.Size = new System.Drawing.Size(65, 30);
        //     this.btnUpdateUser.Stroke = false;
        //     this.btnUpdateUser.StrokeColor = System.Drawing.Color.Gray;
        //     this.btnUpdateUser.TabIndex = 9;
        //     this.btnUpdateUser.Text = "Editar";
        //     this.btnUpdateUser.Transparency = false;
        //     this.btnUpdateUser.Click += new System.EventHandler(this.btnUpdateUser_Click);
        //     // 
        //     // btnEliminar
        //     // 
        //     this.btnEliminar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
        //     this.btnEliminar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnEliminar.BackColor = System.Drawing.Color.Transparent;
        //     this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
        //     this.btnEliminar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
        //     this.btnEliminar.ForeColor = System.Drawing.Color.White;
        //     this.btnEliminar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnEliminar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        //     this.btnEliminar.Location = new System.Drawing.Point(244, 383);
        //     this.btnEliminar.Name = "btnEliminar";
        //     this.btnEliminar.Radius = 10;
        //     this.btnEliminar.Size = new System.Drawing.Size(65, 30);
        //     this.btnEliminar.Stroke = false;
        //     this.btnEliminar.StrokeColor = System.Drawing.Color.Gray;
        //     this.btnEliminar.TabIndex = 10;
        //     this.btnEliminar.Text = "Eliminar";
        //     this.btnEliminar.Transparency = false;
        //     this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
        //     // 
        //     // FrmUsuarios
        //     // 
        //     this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //     this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //     this.ClientSize = new System.Drawing.Size(483, 450);
        //     this.Controls.Add(this.btnEliminar);
        //     this.Controls.Add(this.btnUpdateUser);
        //     this.Controls.Add(this.DgvUsers);
        //     this.Controls.Add(this.btnSearchUser);
        //     this.Controls.Add(this.txtDocumento);
        //     this.Controls.Add(this.btnAddUser);
        //     this.Controls.Add(this.labUsuarios);
        //     this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        //     this.Name = "FrmUsuarios";
        //     this.Text = "FrmUsuarios";
        //     this.Load += new System.EventHandler(this.FrmUsuarios_Load);
        //     ((System.ComponentModel.ISupportInitialize)(this.DgvUsers)).EndInit();
        //     this.ResumeLayout(false);
        //
        // }

        #endregion

        //  private AltoControls.AltoSlidingLabel labUsuarios;
        //  private AltoControls.AltoButton btnAddUser;
        //  private AltoControls.AltoTextBox txtDocumento;
        //  private AltoControls.AltoButton btnSearchUser;
        //  private System.Windows.Forms.DataGridView DgvUsers;
        //  private AltoControls.AltoButton btnUpdateUser;
        //  private AltoControls.AltoButton btnEliminar;
    }
}