﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class E_Login
    {
        public int Codigo{ get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
    }   
}
