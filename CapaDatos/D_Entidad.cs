﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidad;
namespace CapaDatos
{
    public class D_Entidad
    {
        SqlCommand objComando = new SqlCommand();
        public E_Entidad MostrarPerfil(int codigoEntidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            //esta instancia sera para guardar datos y corroborar si realmente son los mismo para dar acceso 
            E_Entidad e_Entidad = new E_Entidad();
            try
            {
                //comentattft dfdf

                objComando.CommandText = "view_EntidadPerfil";
                //objComando.CommandType = CommandType.StoredProcedure;
                //objComando.Connection = objConexion;
                objComando.Parameters.Clear();
                objComando.Connection.Open();
                objComando.Parameters.AddWithValue("@codigo", codigoEntidad);
                SqlDataReader dataReader = objComando.ExecuteReader();

                if (dataReader.HasRows)
                {
                    //while ()
                    //{
                    dataReader.Read();
                    e_Entidad.Codigo = Convert.ToInt32(dataReader["Codigo"]);
                    e_Entidad.Documento = Convert.ToString(dataReader["Documento"]);
                    e_Entidad.Nombres = Convert.ToString(dataReader["Nombres"]);
                    e_Entidad.ApellidoPaterno = Convert.ToString(dataReader["ApellidoPaterno"]);
                    e_Entidad.ApellidoMaterno = Convert.ToString(dataReader["ApellidoMaterno"]);
                    e_Entidad.NumeroTelefonico = Convert.ToString(dataReader["NumeroTelefonico"]);
                    e_Entidad.Usuario = Convert.ToString(dataReader["Usuario"]);
                    e_Entidad.IdUbigeo = Convert.ToInt16(dataReader["ubigeoCodigo"]);
                    e_Entidad.IdUbigeoCodigoPadre = Convert.ToInt16(dataReader["UbigeoCodigoPadre"]);
                    e_Entidad.Direccion = Convert.ToString(dataReader["Direccion"]);
                    e_Entidad.TipoEntidad = Convert.ToString(dataReader["TipoUsuario"]);
                    //}
                }
                objComando.Connection.Close();
                return e_Entidad;
            }
            catch (Exception)
            {
                throw new Exception("Hubo un error al visualizar los datos de perfil");
            }

        }
        public Boolean ActualizarPerfil(E_Entidad entidad)
        {
            objComando = new D_SqlClient().ConstruirComando();
            //esta instancia sera para guardar datos y corroborar si realmente son los mismo para dar acceso 
            E_Entidad e_Entidad = new E_Entidad();
            try
            {

                objComando.CommandText = "Update_PerfilEntidad";
                objComando.Parameters.Clear();
                objComando.Connection.Open();
                objComando.Parameters.AddWithValue("@codigo", entidad.Codigo);
                objComando.Parameters.AddWithValue("@Usuario", entidad.Usuario);
                objComando.Parameters.AddWithValue("@Nombres", entidad.Nombres);
                objComando.Parameters.AddWithValue("@ApellidoPaterno", entidad.ApellidoPaterno);
                objComando.Parameters.AddWithValue("@ApellidoMaterno", entidad.ApellidoMaterno);
                objComando.Parameters.AddWithValue("@NumeroTelefonico", entidad.NumeroTelefonico);
                objComando.ExecuteNonQuery();
                objComando.Connection.Close();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Error al actualizar el perfil de " + entidad.Nombres);
            }
        }

        public Boolean InsertarEntidad(E_Entidad e_Entidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            objComando.Connection.Open();
            //SqlTransaction objTran = objComando.Connection.BeginTransaction(); // inicio del ambito de transaccion
            try
            {
                //objComando.Transaction = objTran;
                objComando.Parameters.Clear();
                if (e_Entidad.TipoEntidad == "colaborador")
                {
                    objComando.CommandText = "Insert_EntidadColaborador";
                    objComando.Parameters.AddWithValue("@RolAdmin", e_Entidad.RolAdmin);
                }
                else
                {
                    objComando.CommandText = "Insert_EntidadCliente";
                    objComando.Parameters.AddWithValue("@Puntos", e_Entidad.Puntos);
                }
                objComando.Parameters.AddWithValue("@IdDocumento", e_Entidad.IdDocumento);
                objComando.Parameters.AddWithValue("@IdUbigeo", e_Entidad.IdUbigeo);
                objComando.Parameters.AddWithValue("@Nombres", e_Entidad.Nombres);
                objComando.Parameters.AddWithValue("@ApellidoPaterno", e_Entidad.ApellidoPaterno);
                objComando.Parameters.AddWithValue("@ApellidoMaterno", e_Entidad.ApellidoMaterno);
                objComando.Parameters.AddWithValue("@Correo", e_Entidad.Usuario);
                objComando.Parameters.AddWithValue("@Password", e_Entidad.Password);
                objComando.Parameters.AddWithValue("@NumeroTelefonico", e_Entidad.NumeroTelefonico);
                objComando.Parameters.AddWithValue("@RutaImagen", e_Entidad.RutaImagen);
                objComando.Parameters.AddWithValue("@FechaNacimiento", e_Entidad.FechaNacimiento);
                objComando.Parameters.AddWithValue("@Documento", e_Entidad.Documento);
                objComando.Parameters.AddWithValue("@Activo", e_Entidad.Activo);

                objComando.ExecuteNonQuery();

                //objTran.Commit(); //fin del ambito de transaccion

                objComando.Connection.Close();
                return true;
            }
            catch (SqlException sqlex)
            //catch (Exception)
            {
                //objTran.Rollback();
                throw new Exception(sqlex.Message);
            }

        }
        public Boolean UpdateEntidad(E_Entidad e_Entidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            objComando.Connection.Open();
            //SqlTransaction objTran = objComando.Connection.BeginTransaction(); // inicio del ambito de transaccion
            try
            {
                //   objComando.Transaction = objTran;
                objComando.Parameters.Clear();
                if (e_Entidad.TipoEntidad == "colaborador")
                {
                    objComando.CommandText = "Update_EntidadColaborador";
                    objComando.Parameters.AddWithValue("@RolAdmin", e_Entidad.RolAdmin);
                }
                else
                {
                    objComando.CommandText = "Update_EntidadCliente";
                    objComando.Parameters.AddWithValue("@Puntos", e_Entidad.Puntos);
                }
                objComando.Parameters.AddWithValue("@Codigo", e_Entidad.Codigo);
                objComando.Parameters.AddWithValue("@IdDocumento", e_Entidad.IdDocumento);
                objComando.Parameters.AddWithValue("@IdUbigeo", e_Entidad.IdUbigeo);
                objComando.Parameters.AddWithValue("@Nombres", e_Entidad.Nombres);
                objComando.Parameters.AddWithValue("@ApellidoPaterno", e_Entidad.ApellidoPaterno);
                objComando.Parameters.AddWithValue("@ApellidoMaterno", e_Entidad.ApellidoMaterno);
                objComando.Parameters.AddWithValue("@Correo", e_Entidad.Usuario);
                objComando.Parameters.AddWithValue("@Password", e_Entidad.Password);
                objComando.Parameters.AddWithValue("@NumeroTelefonico", e_Entidad.NumeroTelefonico);
                objComando.Parameters.AddWithValue("@RutaImagen", e_Entidad.RutaImagen);
                objComando.Parameters.AddWithValue("@FechaNacimiento", e_Entidad.FechaNacimiento);
                objComando.Parameters.AddWithValue("@Documento", e_Entidad.Documento);
                objComando.Parameters.AddWithValue("@Activo", e_Entidad.Activo);

                objComando.ExecuteNonQuery();

                //objTran.Commit(); //fin del ambito de transaccion

                objComando.Connection.Close();
                return true;
            }
            //catch (Exception)
            catch (SqlException sqlex)
            {
                //objTran.Rollback();
                throw new Exception(sqlex.Message);
            }

        }
        public Boolean EliminarEntidad(E_Entidad e_Entidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            objComando.Connection.Open();
            //SqlTransaction objTran = objComando.Connection.BeginTransaction(); // inicio del ambito de transaccion
            try
            {
                //  objComando.Transaction = objTran;
                objComando.Parameters.Clear();
                if (e_Entidad.TipoEntidad == "colaborador")
                {
                    objComando.CommandText = "Delete_EntidadColaborador";

                }
                else
                {
                    objComando.CommandText = "Delete_EntidadCliente";

                }
                objComando.Parameters.AddWithValue("@CodigoEntidad", e_Entidad.Codigo);


                objComando.ExecuteNonQuery();

                //objTran.Commit(); //fin del ambito de transaccion

                objComando.Connection.Close();
                return true;
            }
            catch (SqlException sqlex)
            {
                //objTran.Rollback();
                throw new Exception(sqlex.Message);
            }

        }
        public DataTable ListUsers()
        {
            objComando = new D_SqlClient().ConstruirComando();
            objComando.CommandText = "view_ListUsers";
            objComando.Parameters.Clear();
            objComando.Connection.Open();
            //objComando.ExecuteNonQuery(); Insert. Delete, Update
            SqlDataReader reader;
            reader = objComando.ExecuteReader(); //Select
            DataTable tabla = new DataTable();
            tabla.Load(reader);
            objComando.Connection.Close();
            return tabla;
        }
        public DataTable AyudaEntidadXDoc(string Documento)
        {
            objComando = new D_SqlClient().ConstruirComando();
            objComando.CommandText = "Ayuda_ENTIDAD_NumDoc";
            objComando.Parameters.Clear();
            objComando.Connection.Open();
            objComando.Parameters.AddWithValue("@Documento", Documento);
            //objComando.ExecuteNonQuery(); Insert. Delete, Update
            SqlDataReader reader;
            reader = objComando.ExecuteReader(); //Select
            DataTable tabla = new DataTable();
            tabla.Load(reader);
            objComando.Connection.Close();
            return tabla;
        }
        public Boolean RegistrarColaborador(E_Entidad e_Entidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            objComando.Connection.Open();
            //SqlTransaction objTran = objComando.Connection.BeginTransaction(); // inicio del ambito de transaccion
            try
            {
                //objComando.Transaction = objTran;
                objComando.Parameters.Clear();
                objComando.CommandText = "Insert_EntidadColaborador";
                objComando.Parameters.AddWithValue("@IdDocumento", e_Entidad.IdDocumento);
                objComando.Parameters.AddWithValue("@IdUbigeo", e_Entidad.IdUbigeo);
                objComando.Parameters.AddWithValue("@Nombres", e_Entidad.Nombres);
                objComando.Parameters.AddWithValue("@ApellidoPaterno", e_Entidad.ApellidoPaterno);
                objComando.Parameters.AddWithValue("@ApellidoMaterno", e_Entidad.ApellidoMaterno);
                objComando.Parameters.AddWithValue("@Correo", e_Entidad.Usuario);
                objComando.Parameters.AddWithValue("@Domicilio", e_Entidad.Direccion);
                objComando.Parameters.AddWithValue("@Password", e_Entidad.Password);
                objComando.Parameters.AddWithValue("@NumeroTelefonico", e_Entidad.NumeroTelefonico);
                objComando.Parameters.AddWithValue("@RutaImagen", "");
                objComando.Parameters.AddWithValue("@FechaNacimiento", e_Entidad.FechaNacimiento);
                objComando.Parameters.AddWithValue("@Documento", e_Entidad.Documento);
                objComando.Parameters.AddWithValue("@Activo", 1);

                objComando.ExecuteNonQuery();

                //objTran.Commit(); //fin del ambito de transaccion

                objComando.Connection.Close();
                return true;
            }
            catch (SqlException sqlex)
            //catch (Exception)
            {
                //objTran.Rollback();
                throw new Exception(sqlex.Message);
            }

        }



        public Boolean RegistrarCliente(E_Entidad e_Entidad)
        {

            objComando = new D_SqlClient().ConstruirComando();
            objComando.Connection.Open();
            //SqlTransaction objTran = objComando.Connection.BeginTransaction(); // inicio del ambito de transaccion
            try
            {
                //objComando.Transaction = objTran;
                objComando.Parameters.Clear();
                objComando.CommandText = "Insert_EntidadCliente";
                objComando.Parameters.AddWithValue("@IdDocumento", e_Entidad.IdDocumento);
                objComando.Parameters.AddWithValue("@IdUbigeo", e_Entidad.IdUbigeo);
                objComando.Parameters.AddWithValue("@Nombres", e_Entidad.Nombres);
                objComando.Parameters.AddWithValue("@ApellidoPaterno", e_Entidad.ApellidoPaterno);
                objComando.Parameters.AddWithValue("@ApellidoMaterno", e_Entidad.ApellidoMaterno);
                objComando.Parameters.AddWithValue("@Correo", e_Entidad.Usuario);
              //  objComando.Parameters.AddWithValue("@Domicilio", e_Entidad.Direccion);
                objComando.Parameters.AddWithValue("@Password", e_Entidad.Password);
                objComando.Parameters.AddWithValue("@NumeroTelefonico", e_Entidad.NumeroTelefonico);
                objComando.Parameters.AddWithValue("@RutaImagen", "");
                //objComando.Parameters.AddWithValue("@FechaNacimiento", e_Entidad.FechaNacimiento);
                objComando.Parameters.AddWithValue("@Documento", e_Entidad.Documento);
                objComando.Parameters.AddWithValue("@Activo", 0);
                objComando.Parameters.AddWithValue("@Puntos",999);

                objComando.ExecuteNonQuery();

                //objTran.Commit(); //fin del ambito de transaccion

                objComando.Connection.Close();
                return true;
            }
            catch (SqlException sqlex)
            //catch (Exception)
            {
                //objTran.Rollback();
                throw new Exception(sqlex.Message);
            }

        }




    }
}
