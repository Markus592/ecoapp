﻿
namespace WindowsFormsApp1
{
    partial class Frm_Recojo_Nuevo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PDetallePedido = new System.Windows.Forms.Panel();
            this.lbDetallePedido = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbAlerta = new System.Windows.Forms.Label();
            this.PDescripcion = new System.Windows.Forms.Panel();
            this.lbDescripcion = new System.Windows.Forms.Label();
            this.cbMateriales = new System.Windows.Forms.ComboBox();
            this.dtFechaRecojo = new System.Windows.Forms.DateTimePicker();
            this.txtPesoAprox = new AltoControls.AltoTextBox();
            this.altoButton1 = new AltoControls.AltoButton();
            this.txtDescripcion = new AltoControls.AltoTextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.PDetallePedido.SuspendLayout();
            this.PDescripcion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // PDetallePedido
            // 
            this.PDetallePedido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PDetallePedido.BackColor = System.Drawing.Color.SeaGreen;
            this.PDetallePedido.Controls.Add(this.lbDetallePedido);
            this.PDetallePedido.Location = new System.Drawing.Point(1, 12);
            this.PDetallePedido.Name = "PDetallePedido";
            this.PDetallePedido.Size = new System.Drawing.Size(846, 44);
            this.PDetallePedido.TabIndex = 10;
            // 
            // lbDetallePedido
            // 
            this.lbDetallePedido.AutoSize = true;
            this.lbDetallePedido.BackColor = System.Drawing.Color.Transparent;
            this.lbDetallePedido.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbDetallePedido.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbDetallePedido.Location = new System.Drawing.Point(3, 15);
            this.lbDetallePedido.Name = "lbDetallePedido";
            this.lbDetallePedido.Size = new System.Drawing.Size(167, 19);
            this.lbDetallePedido.TabIndex = 9;
            this.lbDetallePedido.Text = "DETALLE DEL PEDIDO";
            this.lbDetallePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.SeaGreen;
            this.label7.Location = new System.Drawing.Point(502, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 19);
            this.label7.TabIndex = 18;
            this.label7.Text = "FECHA RECOJO";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.SeaGreen;
            this.label9.Location = new System.Drawing.Point(279, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "MATERIALES";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.SeaGreen;
            this.label10.Location = new System.Drawing.Point(87, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 19);
            this.label10.TabIndex = 15;
            this.label10.Text = "PESO APROX";
            // 
            // lbAlerta
            // 
            this.lbAlerta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbAlerta.AutoSize = true;
            this.lbAlerta.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbAlerta.ForeColor = System.Drawing.Color.Red;
            this.lbAlerta.Location = new System.Drawing.Point(289, 607);
            this.lbAlerta.Name = "lbAlerta";
            this.lbAlerta.Size = new System.Drawing.Size(314, 19);
            this.lbAlerta.TabIndex = 20;
            this.lbAlerta.Text = "ELIMINAR EL RECOJO 24 HORAS ANTES ";
            // 
            // PDescripcion
            // 
            this.PDescripcion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PDescripcion.BackColor = System.Drawing.Color.SeaGreen;
            this.PDescripcion.Controls.Add(this.lbDescripcion);
            this.PDescripcion.Location = new System.Drawing.Point(1, 255);
            this.PDescripcion.Name = "PDescripcion";
            this.PDescripcion.Size = new System.Drawing.Size(846, 44);
            this.PDescripcion.TabIndex = 11;
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.AutoSize = true;
            this.lbDescripcion.BackColor = System.Drawing.Color.Transparent;
            this.lbDescripcion.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbDescripcion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbDescripcion.Location = new System.Drawing.Point(3, 9);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.Size = new System.Drawing.Size(110, 19);
            this.lbDescripcion.TabIndex = 9;
            this.lbDescripcion.Text = "DESCRIPCIÓN";
            this.lbDescripcion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbDescripcion.Click += new System.EventHandler(this.label13_Click);
            // 
            // cbMateriales
            // 
            this.cbMateriales.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbMateriales.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbMateriales.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cbMateriales.FormattingEnabled = true;
            this.cbMateriales.Location = new System.Drawing.Point(282, 147);
            this.cbMateriales.Name = "cbMateriales";
            this.cbMateriales.Size = new System.Drawing.Size(121, 21);
            this.cbMateriales.TabIndex = 22;
            // 
            // dtFechaRecojo
            // 
            this.dtFechaRecojo.Location = new System.Drawing.Point(495, 144);
            this.dtFechaRecojo.Name = "dtFechaRecojo";
            this.dtFechaRecojo.Size = new System.Drawing.Size(200, 20);
            this.dtFechaRecojo.TabIndex = 23;
            // 
            // txtPesoAprox
            // 
            this.txtPesoAprox.BackColor = System.Drawing.Color.Transparent;
            this.txtPesoAprox.Br = System.Drawing.Color.White;
            this.txtPesoAprox.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtPesoAprox.ForeColor = System.Drawing.Color.FloralWhite;
            this.txtPesoAprox.Location = new System.Drawing.Point(90, 144);
            this.txtPesoAprox.Name = "txtPesoAprox";
            this.txtPesoAprox.PasswordChar = false;
            this.txtPesoAprox.Size = new System.Drawing.Size(135, 33);
            this.txtPesoAprox.TabIndex = 24;
            // 
            // altoButton1
            // 
            this.altoButton1.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.altoButton1.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.altoButton1.BackColor = System.Drawing.Color.Transparent;
            this.altoButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.altoButton1.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.altoButton1.ForeColor = System.Drawing.Color.White;
            this.altoButton1.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.altoButton1.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.altoButton1.Location = new System.Drawing.Point(678, 607);
            this.altoButton1.Name = "altoButton1";
            this.altoButton1.Radius = 10;
            this.altoButton1.Size = new System.Drawing.Size(151, 40);
            this.altoButton1.Stroke = false;
            this.altoButton1.StrokeColor = System.Drawing.Color.Gray;
            this.altoButton1.TabIndex = 25;
            this.altoButton1.Text = "Agregar";
            this.altoButton1.Transparency = false;
            this.altoButton1.Click += new System.EventHandler(this.altoButton1_Click);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.BackColor = System.Drawing.Color.Transparent;
            this.txtDescripcion.Br = System.Drawing.SystemColors.ControlDark;
            this.txtDescripcion.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtDescripcion.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.txtDescripcion.Location = new System.Drawing.Point(43, 330);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.PasswordChar = false;
            this.txtDescripcion.Size = new System.Drawing.Size(747, 219);
            this.txtDescripcion.TabIndex = 26;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Frm_Recojo_Nuevo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(841, 674);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.altoButton1);
            this.Controls.Add(this.txtPesoAprox);
            this.Controls.Add(this.dtFechaRecojo);
            this.Controls.Add(this.PDescripcion);
            this.Controls.Add(this.cbMateriales);
            this.Controls.Add(this.lbAlerta);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.PDetallePedido);
            this.ForeColor = System.Drawing.Color.SeaGreen;
            this.Name = "Frm_Recojo_Nuevo";
            this.Text = "Frm_Recojo";
            this.Load += new System.EventHandler(this.Frm_Recojo_Load);
            this.PDetallePedido.ResumeLayout(false);
            this.PDetallePedido.PerformLayout();
            this.PDescripcion.ResumeLayout(false);
            this.PDescripcion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel PDetallePedido;
        private System.Windows.Forms.Label lbDetallePedido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbAlerta;
        private System.Windows.Forms.Panel PDescripcion;
        private System.Windows.Forms.Label lbDescripcion;
        private System.Windows.Forms.ComboBox cbMateriales;
        private System.Windows.Forms.DateTimePicker dtFechaRecojo;
        private AltoControls.AltoTextBox txtPesoAprox;
        private AltoControls.AltoButton altoButton1;
        private AltoControls.AltoTextBox txtDescripcion;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}