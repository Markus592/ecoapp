﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidad;
namespace CapaDatos
{
    public class D_TipoDocumento
    {
        SqlCommand objComando = new SqlCommand();
        public DataTable MostrarTipoDocumento() { 
        objComando = new D_SqlClient().ConstruirComando();
            try
            {

                objComando.CommandText = "view_MostrarTipoDocumento";
                
                objComando.Parameters.Clear();
                objComando.Connection.Open();
               
                SqlDataReader dataReader = objComando.ExecuteReader();
       
                DataTable TipoDocumento = new DataTable();
                TipoDocumento.Load(dataReader, LoadOption.OverwriteChanges);
                
                objComando.Connection.Close();
                return TipoDocumento;
            }
            catch (Exception)
            {
                throw new Exception("Hubo un error al sacar los datos de tipos de documentos");
}

        }
    }
}
