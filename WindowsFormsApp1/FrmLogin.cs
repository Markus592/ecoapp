﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using CapaNegocio;
using CapaEntidad;
using System.Runtime.InteropServices;

namespace WindowsFormsApp1
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void altoButton2_Click(object sender, EventArgs e)
        {
            FrmRegistro frm = new FrmRegistro();

            frm.Show();
            this.Hide();
        }

        private void maximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            maximizar.Visible = false;
            restaurar.Visible = true;
        }

        private void minimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void restaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            restaurar.Visible = false;
            maximizar.Visible = true;
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnIniciarSesionClick(object sender, EventArgs e)
        {

            try
            {
                if (N_LoginEntidad.email_bien_escrito(txtusuario.Text))
                {
                    E_Login _Login = new E_Login();

                    _Login.Correo = txtusuario.Text;
                    _Login.Password = Encrypt.GetSHA256( txtcontraseña.Text);
                    _Login.Codigo = N_LoginEntidad.LoginEntidad(_Login);
                    FrmPrincipal main = new FrmPrincipal(_Login.Codigo);
                    main.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("Formato de Correo electrónico incorrecto");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
