﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FrmListarRecojo : Form
    {
        public FrmListarRecojo()
        {
            InitializeComponent();
            DgvRecojo.MultiSelect = false ;
      
        }
      
        public FrmListarRecojo(DataTable objTabla)
        {
            InitializeComponent();
            DgvRecojo.DataSource = objTabla;
            DgvRecojo.MultiSelect = false;
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            

        }

       

        private void txtDocumento_Click(object sender, EventArgs e)
        {
            
        }

        private void btnUpdateUser_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            this.DgvRecojo = new System.Windows.Forms.DataGridView();
            this.btnBuscar = new AltoControls.AltoButton();
            this.lbTituloListaRecojo = new AltoControls.AltoSlidingLabel();
            this.btnActualizar = new AltoControls.AltoButton();
            this.btnEliminar = new AltoControls.AltoButton();
            this.btnAgregar = new AltoControls.AltoButton();
            this.dtpFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaFin = new System.Windows.Forms.DateTimePicker();
            this.lbFechaInicio = new System.Windows.Forms.Label();
            this.lbFechaFin = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRecojo)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvRecojo
            // 
            this.DgvRecojo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvRecojo.Location = new System.Drawing.Point(69, 193);
            this.DgvRecojo.Name = "DgvRecojo";
            this.DgvRecojo.Size = new System.Drawing.Size(494, 222);
            this.DgvRecojo.TabIndex = 15;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnBuscar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnBuscar.BackColor = System.Drawing.Color.Transparent;
            this.btnBuscar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnBuscar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBuscar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBuscar.Location = new System.Drawing.Point(69, 145);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Radius = 10;
            this.btnBuscar.Size = new System.Drawing.Size(77, 30);
            this.btnBuscar.Stroke = false;
            this.btnBuscar.StrokeColor = System.Drawing.Color.Gray;
            this.btnBuscar.TabIndex = 12;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Transparency = false;
            // 
            // lbTituloListaRecojo
            // 
            this.lbTituloListaRecojo.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTituloListaRecojo.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbTituloListaRecojo.Location = new System.Drawing.Point(69, 11);
            this.lbTituloListaRecojo.Margin = new System.Windows.Forms.Padding(2);
            this.lbTituloListaRecojo.Name = "lbTituloListaRecojo";
            this.lbTituloListaRecojo.Size = new System.Drawing.Size(225, 60);
            this.lbTituloListaRecojo.Slide = false;
            this.lbTituloListaRecojo.TabIndex = 11;
            this.lbTituloListaRecojo.Text = "Lista de Recojos";
            this.lbTituloListaRecojo.Click += new System.EventHandler(this.labUsuarios_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnActualizar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnActualizar.BackColor = System.Drawing.Color.Transparent;
            this.btnActualizar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnActualizar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnActualizar.ForeColor = System.Drawing.Color.White;
            this.btnActualizar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizar.Location = new System.Drawing.Point(383, 450);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Radius = 10;
            this.btnActualizar.Size = new System.Drawing.Size(112, 39);
            this.btnActualizar.Stroke = false;
            this.btnActualizar.StrokeColor = System.Drawing.Color.Gray;
            this.btnActualizar.TabIndex = 43;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.Transparency = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnEliminar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEliminar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEliminar.Location = new System.Drawing.Point(236, 450);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Radius = 10;
            this.btnEliminar.Size = new System.Drawing.Size(112, 39);
            this.btnEliminar.Stroke = false;
            this.btnEliminar.StrokeColor = System.Drawing.Color.Gray;
            this.btnEliminar.TabIndex = 44;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Transparency = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnAgregar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnAgregar.BackColor = System.Drawing.Color.Transparent;
            this.btnAgregar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAgregar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnAgregar.ForeColor = System.Drawing.Color.White;
            this.btnAgregar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.Location = new System.Drawing.Point(69, 450);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Radius = 10;
            this.btnAgregar.Size = new System.Drawing.Size(112, 39);
            this.btnAgregar.Stroke = false;
            this.btnAgregar.StrokeColor = System.Drawing.Color.Gray;
            this.btnAgregar.TabIndex = 45;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.Transparency = false;
            // 
            // dtpFechaInicio
            // 
            this.dtpFechaInicio.Location = new System.Drawing.Point(69, 103);
            this.dtpFechaInicio.Name = "dtpFechaInicio";
            this.dtpFechaInicio.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaInicio.TabIndex = 46;
            // 
            // dtpFechaFin
            // 
            this.dtpFechaFin.Location = new System.Drawing.Point(287, 103);
            this.dtpFechaFin.Name = "dtpFechaFin";
            this.dtpFechaFin.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaFin.TabIndex = 47;
            // 
            // lbFechaInicio
            // 
            this.lbFechaInicio.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbFechaInicio.AutoSize = true;
            this.lbFechaInicio.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbFechaInicio.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbFechaInicio.Location = new System.Drawing.Point(68, 73);
            this.lbFechaInicio.Name = "lbFechaInicio";
            this.lbFechaInicio.Size = new System.Drawing.Size(118, 19);
            this.lbFechaInicio.TabIndex = 48;
            this.lbFechaInicio.Text = "FECHA INICIO";
            // 
            // lbFechaFin
            // 
            this.lbFechaFin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbFechaFin.AutoSize = true;
            this.lbFechaFin.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbFechaFin.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbFechaFin.Location = new System.Drawing.Point(286, 73);
            this.lbFechaFin.Name = "lbFechaFin";
            this.lbFechaFin.Size = new System.Drawing.Size(91, 19);
            this.lbFechaFin.TabIndex = 49;
            this.lbFechaFin.Text = "FECHA FIN";
            // 
            // FrmListarRecojo
            // 
            this.ClientSize = new System.Drawing.Size(669, 524);
            this.Controls.Add(this.lbFechaFin);
            this.Controls.Add(this.lbFechaInicio);
            this.Controls.Add(this.dtpFechaFin);
            this.Controls.Add(this.dtpFechaInicio);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.DgvRecojo);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.lbTituloListaRecojo);
            this.Name = "FrmListarRecojo";
            this.Text = "LISTA DE RECOJOS";
            this.Load += new System.EventHandler(this.FrmUsuarios_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.DgvRecojo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void FrmUsuarios_Load_1(object sender, EventArgs e)
        {

        }

        private void labUsuarios_Click(object sender, EventArgs e)
        {

        }
    }
}
