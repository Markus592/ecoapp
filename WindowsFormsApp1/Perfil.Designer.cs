﻿
namespace WindowsFormsApp1
{
    partial class Perfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Perfil));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtDomicilio = new AltoControls.AltoTextBox();
            this.txtCelular = new AltoControls.AltoTextBox();
            this.txtCorreo = new AltoControls.AltoTextBox();
            this.txtApMaterno = new AltoControls.AltoTextBox();
            this.txtApPaterno = new AltoControls.AltoTextBox();
            this.txtNombre = new AltoControls.AltoTextBox();
            this.btnActualizarPerfil = new AltoControls.AltoButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(211, 45);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.BackColor = System.Drawing.Color.Transparent;
            this.txtDomicilio.Br = System.Drawing.Color.White;
            this.txtDomicilio.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtDomicilio.ForeColor = System.Drawing.Color.DimGray;
            this.txtDomicilio.Location = new System.Drawing.Point(124, 270);
            this.txtDomicilio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.PasswordChar = false;
            this.txtDomicilio.Size = new System.Drawing.Size(347, 22);
            this.txtDomicilio.TabIndex = 16;
            this.txtDomicilio.Text = "Domicilio";
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.Transparent;
            this.txtCelular.Br = System.Drawing.Color.White;
            this.txtCelular.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtCelular.ForeColor = System.Drawing.Color.DimGray;
            this.txtCelular.Location = new System.Drawing.Point(124, 311);
            this.txtCelular.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.PasswordChar = false;
            this.txtCelular.Size = new System.Drawing.Size(150, 22);
            this.txtCelular.TabIndex = 15;
            this.txtCelular.Text = "Celular";
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.Color.Transparent;
            this.txtCorreo.Br = System.Drawing.Color.White;
            this.txtCorreo.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtCorreo.ForeColor = System.Drawing.Color.DimGray;
            this.txtCorreo.Location = new System.Drawing.Point(294, 311);
            this.txtCorreo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PasswordChar = false;
            this.txtCorreo.Size = new System.Drawing.Size(164, 22);
            this.txtCorreo.TabIndex = 14;
            this.txtCorreo.Text = "Correo";
            // 
            // txtApMaterno
            // 
            this.txtApMaterno.BackColor = System.Drawing.Color.Transparent;
            this.txtApMaterno.Br = System.Drawing.Color.White;
            this.txtApMaterno.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtApMaterno.ForeColor = System.Drawing.Color.DimGray;
            this.txtApMaterno.Location = new System.Drawing.Point(294, 229);
            this.txtApMaterno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApMaterno.Name = "txtApMaterno";
            this.txtApMaterno.PasswordChar = false;
            this.txtApMaterno.Size = new System.Drawing.Size(150, 22);
            this.txtApMaterno.TabIndex = 13;
            this.txtApMaterno.Text = "ApellidoMaterno";
            // 
            // txtApPaterno
            // 
            this.txtApPaterno.BackColor = System.Drawing.Color.Transparent;
            this.txtApPaterno.Br = System.Drawing.Color.White;
            this.txtApPaterno.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtApPaterno.ForeColor = System.Drawing.Color.DimGray;
            this.txtApPaterno.Location = new System.Drawing.Point(124, 229);
            this.txtApPaterno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtApPaterno.Name = "txtApPaterno";
            this.txtApPaterno.PasswordChar = false;
            this.txtApPaterno.Size = new System.Drawing.Size(150, 22);
            this.txtApPaterno.TabIndex = 12;
            this.txtApPaterno.Text = "ApellidoPaterno";
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.Transparent;
            this.txtNombre.Br = System.Drawing.Color.White;
            this.txtNombre.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtNombre.ForeColor = System.Drawing.Color.DimGray;
            this.txtNombre.Location = new System.Drawing.Point(124, 193);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PasswordChar = false;
            this.txtNombre.Size = new System.Drawing.Size(150, 22);
            this.txtNombre.TabIndex = 11;
            this.txtNombre.Text = "Nombre";
            // 
            // btnActualizarPerfil
            // 
            this.btnActualizarPerfil.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnActualizarPerfil.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnActualizarPerfil.BackColor = System.Drawing.Color.Transparent;
            this.btnActualizarPerfil.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnActualizarPerfil.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.btnActualizarPerfil.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnActualizarPerfil.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizarPerfil.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizarPerfil.Location = new System.Drawing.Point(216, 362);
            this.btnActualizarPerfil.Name = "btnActualizarPerfil";
            this.btnActualizarPerfil.Radius = 10;
            this.btnActualizarPerfil.Size = new System.Drawing.Size(139, 30);
            this.btnActualizarPerfil.Stroke = false;
            this.btnActualizarPerfil.StrokeColor = System.Drawing.Color.Gray;
            this.btnActualizarPerfil.TabIndex = 17;
            this.btnActualizarPerfil.Text = "Actualizar Perfil";
            this.btnActualizarPerfil.Transparency = false;
            this.btnActualizarPerfil.Click += new System.EventHandler(this.btnActualizarPerfil_Click);
            // 
            // Perfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 464);
            this.Controls.Add(this.btnActualizarPerfil);
            this.Controls.Add(this.txtDomicilio);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtCorreo);
            this.Controls.Add(this.txtApMaterno);
            this.Controls.Add(this.txtApPaterno);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Perfil";
            this.Text = "Perfil";
            this.Load += new System.EventHandler(this.Perfil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private AltoControls.AltoTextBox txtDomicilio;
        private AltoControls.AltoTextBox txtCelular;
        private AltoControls.AltoTextBox txtCorreo;
        private AltoControls.AltoTextBox txtApMaterno;
        private AltoControls.AltoTextBox txtApPaterno;
        private AltoControls.AltoTextBox txtNombre;
        private AltoControls.AltoButton btnActualizarPerfil;
    }
}