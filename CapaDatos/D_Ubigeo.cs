﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidad;
namespace CapaDatos
{
    public class D_Ubigeo
    {
        SqlCommand objComando = new SqlCommand();
        public DataTable  MostrarDepartamentos()
        {

            objComando = new D_SqlClient().ConstruirComando();
            
            //List<String> Departamento = new List<string>();
            try
            {

                objComando.CommandText = "Ayuda_Ubigeo_Departamento";
                
                objComando.Parameters.Clear();
                objComando.Connection.Open();
                SqlDataReader dataReader = objComando.ExecuteReader();
                //objComando.Connection.Open();
                //objComando.ExecuteNonQuery();
                //if (dataReader.HasRows)
                //{
                //    while (dataReader.Read())
                //    {
                //        Departamento.Add(dataReader.GetString(1));

                //    }
                //}
                
                DataTable Departamento = new DataTable();
                Departamento.Load(dataReader, LoadOption.OverwriteChanges);
                
                objComando.Connection.Close();
                return Departamento;
            }
            catch (Exception)
            {
                throw new Exception("Hubo un error al sacar los datos de departamento");
            }

        }
        public DataTable MostrarProvincias(int codigoDepartamento)
        {

            objComando = new D_SqlClient().ConstruirComando();

            
            try
            {

                objComando.CommandText = "Ayuda_Ubigeo_Provincia";

                objComando.Parameters.Clear();
                objComando.Connection.Open();
                objComando.Parameters.AddWithValue("@ubigeocodigopadre", codigoDepartamento);
               SqlDataReader dataReader = objComando.ExecuteReader();
                DataTable Provincia = new DataTable();
                Provincia.Load(dataReader, LoadOption.OverwriteChanges);
                objComando.Connection.Close();
                return Provincia;
            }
            catch (Exception)
            {
                throw new Exception("Hubo un error al sacar los datos de departamento");
            }

        }
        public DataTable MostrarDistritos(int codigoProvincia)
        {

            objComando = new D_SqlClient().ConstruirComando();
            try
            {

                objComando.CommandText = "Ayuda_Ubigeo_Distrito";

                objComando.Parameters.Clear();
                objComando.Connection.Open();
                objComando.Parameters.AddWithValue("@ubigeocodigopadre", codigoProvincia);
                SqlDataReader dataReader = objComando.ExecuteReader();
                DataTable Distrito = new DataTable();
                Distrito.Load(dataReader, LoadOption.OverwriteChanges);
                objComando.Connection.Close();
                return Distrito;
            }
            catch (Exception)
            {
                throw new Exception("Hubo un error al sacar los datos de departamento");
            }

        }

    }
}
