﻿
namespace WindowsFormsApp1
{
    partial class Recojo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.lbFecha = new System.Windows.Forms.Label();
            this.txtDireccion = new AltoControls.AltoTextBox();
            this.txtCorreo = new AltoControls.AltoTextBox();
            this.txtCelular = new AltoControls.AltoTextBox();
            this.txtApellido = new AltoControls.AltoTextBox();
            this.txtNombre = new AltoControls.AltoTextBox();
            this.lbDireccion = new System.Windows.Forms.Label();
            this.lbCorreo = new System.Windows.Forms.Label();
            this.lbCelular = new System.Windows.Forms.Label();
            this.lbApellido = new System.Windows.Forms.Label();
            this.lbNombre = new System.Windows.Forms.Label();
            this.GrillaMateriales = new System.Windows.Forms.DataGridView();
            this.Peso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Material = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregar = new AltoControls.AltoButton();
            this.btnEliminar = new AltoControls.AltoButton();
            this.btnActualizar = new AltoControls.AltoButton();
            this.btnInsertarRecojo = new AltoControls.AltoButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrillaMateriales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.SeaGreen;
            this.panel1.Controls.Add(this.dtFecha);
            this.panel1.Controls.Add(this.lbFecha);
            this.panel1.Controls.Add(this.txtDireccion);
            this.panel1.Controls.Add(this.txtCorreo);
            this.panel1.Controls.Add(this.txtCelular);
            this.panel1.Controls.Add(this.txtApellido);
            this.panel1.Controls.Add(this.txtNombre);
            this.panel1.Controls.Add(this.lbDireccion);
            this.panel1.Controls.Add(this.lbCorreo);
            this.panel1.Controls.Add(this.lbCelular);
            this.panel1.Controls.Add(this.lbApellido);
            this.panel1.Controls.Add(this.lbNombre);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1114, 111);
            this.panel1.TabIndex = 36;
            // 
            // dtFecha
            // 
            this.dtFecha.Location = new System.Drawing.Point(877, 45);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(200, 20);
            this.dtFecha.TabIndex = 52;
            // 
            // lbFecha
            // 
            this.lbFecha.AutoSize = true;
            this.lbFecha.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbFecha.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbFecha.Location = new System.Drawing.Point(867, 9);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(57, 19);
            this.lbFecha.TabIndex = 51;
            this.lbFecha.Text = "FECHA";
            // 
            // txtDireccion
            // 
            this.txtDireccion.BackColor = System.Drawing.Color.Transparent;
            this.txtDireccion.Br = System.Drawing.Color.White;
            this.txtDireccion.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtDireccion.ForeColor = System.Drawing.Color.DimGray;
            this.txtDireccion.Location = new System.Drawing.Point(651, 45);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.PasswordChar = false;
            this.txtDireccion.Size = new System.Drawing.Size(199, 33);
            this.txtDireccion.TabIndex = 50;
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.Color.Transparent;
            this.txtCorreo.Br = System.Drawing.Color.White;
            this.txtCorreo.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtCorreo.ForeColor = System.Drawing.Color.DimGray;
            this.txtCorreo.Location = new System.Drawing.Point(469, 45);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PasswordChar = false;
            this.txtCorreo.Size = new System.Drawing.Size(164, 33);
            this.txtCorreo.TabIndex = 49;
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.Transparent;
            this.txtCelular.Br = System.Drawing.Color.White;
            this.txtCelular.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtCelular.ForeColor = System.Drawing.Color.DimGray;
            this.txtCelular.Location = new System.Drawing.Point(311, 45);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.PasswordChar = false;
            this.txtCelular.Size = new System.Drawing.Size(125, 33);
            this.txtCelular.TabIndex = 48;
            // 
            // txtApellido
            // 
            this.txtApellido.BackColor = System.Drawing.Color.Transparent;
            this.txtApellido.Br = System.Drawing.Color.White;
            this.txtApellido.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtApellido.ForeColor = System.Drawing.Color.DimGray;
            this.txtApellido.Location = new System.Drawing.Point(142, 45);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.PasswordChar = false;
            this.txtApellido.Size = new System.Drawing.Size(140, 33);
            this.txtApellido.TabIndex = 47;
            this.txtApellido.Click += new System.EventHandler(this.txtApellido_Click);
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.Transparent;
            this.txtNombre.Br = System.Drawing.Color.White;
            this.txtNombre.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtNombre.ForeColor = System.Drawing.Color.DimGray;
            this.txtNombre.Location = new System.Drawing.Point(12, 45);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PasswordChar = false;
            this.txtNombre.Size = new System.Drawing.Size(113, 33);
            this.txtNombre.TabIndex = 46;
            this.txtNombre.Click += new System.EventHandler(this.txtNombre_Click);
            // 
            // lbDireccion
            // 
            this.lbDireccion.AutoSize = true;
            this.lbDireccion.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbDireccion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbDireccion.Location = new System.Drawing.Point(641, 9);
            this.lbDireccion.Name = "lbDireccion";
            this.lbDireccion.Size = new System.Drawing.Size(93, 19);
            this.lbDireccion.TabIndex = 45;
            this.lbDireccion.Text = "DIRECCIÓN";
            // 
            // lbCorreo
            // 
            this.lbCorreo.AutoSize = true;
            this.lbCorreo.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbCorreo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbCorreo.Location = new System.Drawing.Point(459, 9);
            this.lbCorreo.Name = "lbCorreo";
            this.lbCorreo.Size = new System.Drawing.Size(67, 19);
            this.lbCorreo.TabIndex = 43;
            this.lbCorreo.Text = "CORREO";
            // 
            // lbCelular
            // 
            this.lbCelular.AutoSize = true;
            this.lbCelular.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbCelular.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbCelular.Location = new System.Drawing.Point(301, 9);
            this.lbCelular.Name = "lbCelular";
            this.lbCelular.Size = new System.Drawing.Size(157, 19);
            this.lbCelular.TabIndex = 42;
            this.lbCelular.Text = "TELÉFONO/CELULAR";
            // 
            // lbApellido
            // 
            this.lbApellido.AutoSize = true;
            this.lbApellido.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbApellido.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbApellido.Location = new System.Drawing.Point(141, 9);
            this.lbApellido.Name = "lbApellido";
            this.lbApellido.Size = new System.Drawing.Size(90, 19);
            this.lbApellido.TabIndex = 41;
            this.lbApellido.Text = "APELLIDOS";
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.lbNombre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbNombre.Location = new System.Drawing.Point(17, 9);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(70, 19);
            this.lbNombre.TabIndex = 40;
            this.lbNombre.Text = "NOMBRE";
            // 
            // GrillaMateriales
            // 
            this.GrillaMateriales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GrillaMateriales.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.GrillaMateriales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Peso,
            this.Material,
            this.Fecha,
            this.Descripcion});
            this.GrillaMateriales.Location = new System.Drawing.Point(12, 127);
            this.GrillaMateriales.Name = "GrillaMateriales";
            this.GrillaMateriales.Size = new System.Drawing.Size(912, 379);
            this.GrillaMateriales.TabIndex = 0;
            this.GrillaMateriales.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GrillaMateriales_CellContentClick);
            // 
            // Peso
            // 
            this.Peso.HeaderText = "Peso(Kg)";
            this.Peso.Name = "Peso";
            // 
            // Material
            // 
            this.Material.HeaderText = "Material";
            this.Material.Name = "Material";
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            // 
            // Descripcion
            // 
            this.Descripcion.HeaderText = "Descripción";
            this.Descripcion.Name = "Descripcion";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnAgregar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnAgregar.BackColor = System.Drawing.Color.Transparent;
            this.btnAgregar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAgregar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnAgregar.ForeColor = System.Drawing.Color.White;
            this.btnAgregar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.Location = new System.Drawing.Point(965, 207);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Radius = 10;
            this.btnAgregar.Size = new System.Drawing.Size(112, 39);
            this.btnAgregar.Stroke = false;
            this.btnAgregar.StrokeColor = System.Drawing.Color.Gray;
            this.btnAgregar.TabIndex = 42;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.Transparency = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnEliminar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnEliminar.ForeColor = System.Drawing.Color.White;
            this.btnEliminar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEliminar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEliminar.Location = new System.Drawing.Point(965, 274);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Radius = 10;
            this.btnEliminar.Size = new System.Drawing.Size(112, 39);
            this.btnEliminar.Stroke = false;
            this.btnEliminar.StrokeColor = System.Drawing.Color.Gray;
            this.btnEliminar.TabIndex = 42;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Transparency = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnActualizar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnActualizar.BackColor = System.Drawing.Color.Transparent;
            this.btnActualizar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnActualizar.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnActualizar.ForeColor = System.Drawing.Color.White;
            this.btnActualizar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnActualizar.Location = new System.Drawing.Point(965, 341);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Radius = 10;
            this.btnActualizar.Size = new System.Drawing.Size(112, 39);
            this.btnActualizar.Stroke = false;
            this.btnActualizar.StrokeColor = System.Drawing.Color.Gray;
            this.btnActualizar.TabIndex = 42;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.Transparency = false;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnInsertarRecojo
            // 
            this.btnInsertarRecojo.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnInsertarRecojo.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(164)))), ((int)(((byte)(183)))));
            this.btnInsertarRecojo.BackColor = System.Drawing.Color.Transparent;
            this.btnInsertarRecojo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnInsertarRecojo.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
            this.btnInsertarRecojo.ForeColor = System.Drawing.Color.White;
            this.btnInsertarRecojo.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnInsertarRecojo.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnInsertarRecojo.Location = new System.Drawing.Point(965, 422);
            this.btnInsertarRecojo.Name = "btnInsertarRecojo";
            this.btnInsertarRecojo.Radius = 10;
            this.btnInsertarRecojo.Size = new System.Drawing.Size(112, 39);
            this.btnInsertarRecojo.Stroke = false;
            this.btnInsertarRecojo.StrokeColor = System.Drawing.Color.Gray;
            this.btnInsertarRecojo.TabIndex = 43;
            this.btnInsertarRecojo.Text = "Insertar Recojo";
            this.btnInsertarRecojo.Transparency = false;
            this.btnInsertarRecojo.Click += new System.EventHandler(this.btnInsertarRecojo_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Recojo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 542);
            this.Controls.Add(this.btnInsertarRecojo);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.GrillaMateriales);
            this.Controls.Add(this.panel1);
            this.Name = "Recojo";
            this.Text = "Recojo";
            this.Load += new System.EventHandler(this.Recojo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrillaMateriales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbDireccion;
        private System.Windows.Forms.Label lbCorreo;
        private System.Windows.Forms.Label lbCelular;
        private System.Windows.Forms.Label lbApellido;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.DataGridView GrillaMateriales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Peso;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.Label lbFecha;
        private AltoControls.AltoTextBox txtDireccion;
        private AltoControls.AltoTextBox txtCorreo;
        private AltoControls.AltoTextBox txtCelular;
        private AltoControls.AltoTextBox txtApellido;
        private AltoControls.AltoTextBox txtNombre;
        private AltoControls.AltoButton btnAgregar;
        private AltoControls.AltoButton btnEliminar;
        private AltoControls.AltoButton btnActualizar;
        private AltoControls.AltoButton btnInsertarRecojo;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}