﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data;
using CapaDatos;
namespace CapaNegocio
{
    public class N_Ubigeo
    {   
        D_Ubigeo d_Ubigeo = new D_Ubigeo();
        public  DataTable MostrarDepartamentos()
        {
            return d_Ubigeo.MostrarDepartamentos();
        }
        public DataTable MostrarProvincias(int codigoDepartamento)
        {
            return d_Ubigeo.MostrarProvincias(codigoDepartamento);
        }
        public DataTable MostrarDistritos(int codigoDistrito)
        {
            return d_Ubigeo.MostrarDistritos(codigoDistrito);
        }

    }
}
