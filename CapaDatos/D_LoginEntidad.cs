﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidad;

namespace CapaDatos
{
    public class D_LoginEntidad
    {
        SqlCommand objComando = new SqlCommand();
        public E_Login LoginEntidad(E_Login e_Login)
        {
           
            objComando = new D_SqlClient().ConstruirComando();
            //esta instancia sera para guardar datos y corroborar si realmente son los mismo para dar acceso 
            E_Login e_Login1 = new E_Login();
            try
            {
                
                objComando.CommandText = "Login_Entidad";
                objComando.Parameters.Clear();
                objComando.Connection.Open();
                objComando.Parameters.AddWithValue("@Correo", e_Login.Correo);
                objComando.Parameters.AddWithValue("@Password", e_Login.Password);
                SqlDataReader dataReader = objComando.ExecuteReader();
                if (dataReader.HasRows)
                {
                    //while (dataReader.Read())
                    //{
                        dataReader.Read();
                        e_Login1.Codigo = dataReader.GetInt32(0);
                        e_Login1.Correo = dataReader.GetString(1);
                        e_Login1.Password = dataReader.GetString(2);
                    //}
                }
                objComando.Connection.Close();
                return e_Login1;
            }
            catch (Exception )
            {                
                throw new Exception("Hubo un error al momento de Logearse");
            }

        }
    }
}
