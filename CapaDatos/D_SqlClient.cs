using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace CapaDatos
{
    class D_SqlClient
    {
        SqlConnection objConexion = new SqlConnection();
        SqlCommand objComando = new SqlCommand();

        public SqlCommand ConstruirComando()
        {
            //SqlConnectionStringBuilder objBuilder = new SqlConnectionStringBuilder();
            //objBuilder.DataSource = ".";
            //objBuilder.InitialCatalog = "EcoApp";
            //objBuilder.IntegratedSecurity = true;
            string objBuilder =ConfigurationManager.ConnectionStrings[ "EcoApp_sql"].ConnectionString;
            //objBuilder.UserID = "user_DAD";
            //objBuilder.Password = "123456";
            //objConexion.ConnectionString = objBuilder.ConnectionString;
            objConexion.ConnectionString = objBuilder;
            objComando.CommandType = CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            return objComando;
        }

        public SqlCommand ConstruirComando(string strProcedureName)
        {
            SqlConnectionStringBuilder objBuilder = new SqlConnectionStringBuilder();
            objBuilder.DataSource = ".";
            objBuilder.InitialCatalog = "EcoApp";
            objBuilder.IntegratedSecurity = true;
            //objBuilder.UserID = "user_DAD";
            //objBuilder.Password = "123456";
            objConexion.ConnectionString = objBuilder.ConnectionString;
            objComando.CommandType = CommandType.StoredProcedure;
            objComando.Connection = objConexion;
            objComando.CommandText = strProcedureName;
            return objComando;
        }
    }
}
