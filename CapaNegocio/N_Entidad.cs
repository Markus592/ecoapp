﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using System.Data;
using CapaDatos;

using System.Text.RegularExpressions;

namespace CapaNegocio
{
    public class N_Entidad
    {
        D_Entidad d_Entidad = new D_Entidad();
        public E_Entidad MostrarPerfilEntidad(int codigoEntidad)
        {
            return d_Entidad.MostrarPerfil(codigoEntidad);
        }
        public Boolean ActualizarPerfil(E_Entidad entidad)
        {
            if (!N_LoginEntidad.email_bien_escrito(entidad.Usuario))
            {
                throw new Exception("Correo esta mal escrito");
            }
            return d_Entidad.ActualizarPerfil(entidad);
        }
        public DataTable ListUsers()
        {
            return new D_Entidad().ListUsers();
        }
        public DataTable AyudaEntidadXDoc(string objNumDoc)
        {
            return new D_Entidad().AyudaEntidadXDoc(objNumDoc);
        }
        public Boolean RegistrarColaborador(E_Entidad entidad, string txtVerificarContra)
        {
            string excepcion = ValidarCampoColaborador(entidad, txtVerificarContra);
            if (!(excepcion == "Los errores son los siguientes : \n"))
            {
                throw new Exception(excepcion);
            }
            entidad.Password= Encrypt.GetSHA256( entidad.Password);
            return new D_Entidad().RegistrarColaborador(entidad);
        }


        public Boolean RegistrarCliente(E_Entidad entidad, string txtVerificarContra)
        {
            string excepcion = ValidarCampoCliente(entidad, txtVerificarContra);
            if (!(excepcion == "Los errores son los siguientes : \n"))
            {
                throw new Exception(excepcion);
            }
            entidad.Password = Encrypt.GetSHA256(entidad.Password);
            return new D_Entidad().RegistrarColaborador(entidad);
        }




        #region Funciones para validar campos
        public string ValidarCampoColaborador(E_Entidad entidad, string txtVerificarContra)
        {
            string CamposQueFaltan = "";
            string Textinicio = "Los errores son los siguientes : \n";
            CamposQueFaltan += ValidarCamposEntidad(entidad, txtVerificarContra);
            CamposQueFaltan += ValidarDni(entidad.Documento);
            CamposQueFaltan += ValidarNumeroCelular(entidad.NumeroTelefonico);
            CamposQueFaltan += ValidarContraSegura(entidad.Password);
            DateTime currentDateTime = DateTime.Now.Date;
            if ((currentDateTime.Year - entidad.FechaNacimiento.Year) <= 18)
            {
                CamposQueFaltan += "Tiene que ser mayor de edad el colaborador\n";
            }
            if (!String.IsNullOrEmpty(CamposQueFaltan))
            {
                Textinicio += CamposQueFaltan;
            }
            return Textinicio;
        }

        public string ValidarCampoCliente(E_Entidad entidad, string txtVerificarContra)
        {
            string CamposQueFaltan = "";
            string Textinicio = "Los errores son los siguientes : \n";
            CamposQueFaltan += ValidarCamposEntidad(entidad, txtVerificarContra);
            CamposQueFaltan += ValidarDni(entidad.Documento);
            CamposQueFaltan += ValidarNumeroCelular(entidad.NumeroTelefonico);
            CamposQueFaltan += ValidarContraSegura(entidad.Password);
            
            
            if (!String.IsNullOrEmpty(CamposQueFaltan))
            {
                Textinicio += CamposQueFaltan;
            }
            return Textinicio;
        }



        public string ValidarDni(string Documento)
        {
            string mensaje = "";
            var rgx = new Regex(@"^[0-9]{8}$");
            if (!rgx.IsMatch(Documento))
            {
                mensaje = "El formato de DNI es incorrecto tiene que ser asi 99999999\n";
            }
            return mensaje;
        }
        public string ValidarNumeroCelular(string NumeroCelular)
        {
            string mensaje = "";
            var rgx = new Regex(@"^[0-9]{9}$");
            if (!rgx.IsMatch(NumeroCelular))
            {
                mensaje = "El formato de celular es incorrecto tiene que ser asi 999999999\n";
            }
            return mensaje;
        }
        public string ValidarCamposEntidad(E_Entidad entidad, string txtVerificaContra)
        {
            string CamposQueFaltan = "";
            string campoVacio = "Campo vacio ";
            if (String.IsNullOrEmpty(entidad.Nombres))
            {
                CamposQueFaltan += campoVacio + "Nombres\n";
            }
            if (String.IsNullOrEmpty(entidad.ApellidoPaterno))
            {
                CamposQueFaltan += campoVacio + "Apellido Paterno\n";
            }
            if (String.IsNullOrEmpty(entidad.ApellidoMaterno))
            {
                CamposQueFaltan += campoVacio + "Apellido Materno\n";
            }
            if (String.IsNullOrEmpty(entidad.Direccion))
            {
                CamposQueFaltan += campoVacio + "Domicilio\n";
            }
            if (String.IsNullOrEmpty(entidad.Usuario))
            {
                CamposQueFaltan += campoVacio + "Correo\n";
            }
            if (entidad.IdUbigeo == 0)
            {
                CamposQueFaltan += campoVacio + "Distrito\n";
            }
            if (entidad.IdDocumento == 0)
            {
                CamposQueFaltan += campoVacio + "Tipo de Documento\n";
            }
            if (String.IsNullOrEmpty(entidad.Documento))
            {
                CamposQueFaltan += campoVacio + "Documento\n";
            }
            if (String.IsNullOrEmpty(entidad.NumeroTelefonico))
            {
                CamposQueFaltan += campoVacio + "Telefono\n";
            }
            if (String.IsNullOrEmpty(entidad.Password))
            {
                CamposQueFaltan += campoVacio + entidad.Password + "Contraseña\n";
            }
            //validar usuario que sea correcto tipo de formato correo
            if (!email_bien_escrito(entidad.Usuario))
            {
                CamposQueFaltan += "El formato de correo es incorrecto\n";
            }
            if (entidad.Password != txtVerificaContra)
            {
                CamposQueFaltan += "Las contraseñas no coinciden\n";
            }
            return CamposQueFaltan;
        }
        public Boolean email_bien_escrito(String email)
        {

            var expresion = new Regex("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
            if (expresion.IsMatch(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string ValidarContraSegura(string password)
        {
            string mensaje = "";
            var expresion = new Regex(@"^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=.*[!@#$&*]).{8,})\S$");
            if (!expresion.IsMatch(password))
            {
                mensaje = "La contraseña debe tener como mínimo ocho caracteres, al menos una letra Mayuscula y una minuscula con caracteres especiales\n";
            }
            return mensaje;
        }
        #endregion

    }
}
