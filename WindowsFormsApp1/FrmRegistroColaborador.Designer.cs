﻿
namespace WindowsFormsApp1
{
    partial class FrmRegistroColaborador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistroColaborador));
            this.txtNombre = new AltoControls.AltoTextBox();
            this.txtApellidoPaterno = new AltoControls.AltoTextBox();
            this.txtApellidoMaterno = new AltoControls.AltoTextBox();
            this.txtTelefono = new AltoControls.AltoTextBox();
            this.txtPassword = new AltoControls.AltoTextBox();
            this.btnRegistrar = new AltoControls.AltoButton();
            this.txtDomicilio = new AltoControls.AltoTextBox();
            this.cmbDepartamento = new System.Windows.Forms.ComboBox();
            this.cmbProvincia = new System.Windows.Forms.ComboBox();
            this.cmbDistrito = new System.Windows.Forms.ComboBox();
            this.altoSlidingLabel1 = new AltoControls.AltoSlidingLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.restaurar = new System.Windows.Forms.PictureBox();
            this.minimizar = new System.Windows.Forms.PictureBox();
            this.cerrar = new System.Windows.Forms.PictureBox();
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.maximizar = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new AltoControls.AltoTextBox();
            this.txtDocumento = new AltoControls.AltoTextBox();
            this.cmbTipoDocumento = new System.Windows.Forms.ComboBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.txtVerificarContra = new AltoControls.AltoTextBox();
            this.lbNombre = new System.Windows.Forms.Label();
            this.lbApellidoPaterno = new System.Windows.Forms.Label();
            this.lbApellidoMaterno = new System.Windows.Forms.Label();
            this.lbDomicilio = new System.Windows.Forms.Label();
            this.lbDepartamento = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDistrito = new System.Windows.Forms.Label();
            this.lbTipoDocumento = new System.Windows.Forms.Label();
            this.lbDocumento = new System.Windows.Forms.Label();
            this.lbCelular = new System.Windows.Forms.Label();
            this.lbCorreo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbVerificarContrasena = new System.Windows.Forms.Label();
            this.lbFechaNacimiento = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.restaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cerrar)).BeginInit();
            this.BarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maximizar)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.Transparent;
            this.txtNombre.Br = System.Drawing.Color.White;
            this.txtNombre.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtNombre.ForeColor = System.Drawing.Color.DimGray;
            this.txtNombre.Location = new System.Drawing.Point(179, 141);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PasswordChar = false;
            this.txtNombre.Size = new System.Drawing.Size(358, 22);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.Enter += new System.EventHandler(this.txtNombre_Enter);
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.BackColor = System.Drawing.Color.Transparent;
            this.txtApellidoPaterno.Br = System.Drawing.Color.White;
            this.txtApellidoPaterno.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtApellidoPaterno.ForeColor = System.Drawing.Color.DimGray;
            this.txtApellidoPaterno.Location = new System.Drawing.Point(176, 193);
            this.txtApellidoPaterno.Margin = new System.Windows.Forms.Padding(2);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.PasswordChar = false;
            this.txtApellidoPaterno.Size = new System.Drawing.Size(168, 22);
            this.txtApellidoPaterno.TabIndex = 2;
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.BackColor = System.Drawing.Color.Transparent;
            this.txtApellidoMaterno.Br = System.Drawing.Color.White;
            this.txtApellidoMaterno.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtApellidoMaterno.ForeColor = System.Drawing.Color.DimGray;
            this.txtApellidoMaterno.Location = new System.Drawing.Point(360, 193);
            this.txtApellidoMaterno.Margin = new System.Windows.Forms.Padding(2);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.PasswordChar = false;
            this.txtApellidoMaterno.Size = new System.Drawing.Size(177, 22);
            this.txtApellidoMaterno.TabIndex = 3;
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.Transparent;
            this.txtTelefono.Br = System.Drawing.Color.White;
            this.txtTelefono.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtTelefono.ForeColor = System.Drawing.Color.DimGray;
            this.txtTelefono.Location = new System.Drawing.Point(176, 434);
            this.txtTelefono.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.PasswordChar = false;
            this.txtTelefono.Size = new System.Drawing.Size(132, 22);
            this.txtTelefono.TabIndex = 5;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.Transparent;
            this.txtPassword.Br = System.Drawing.Color.White;
            this.txtPassword.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtPassword.ForeColor = System.Drawing.Color.DimGray;
            this.txtPassword.Location = new System.Drawing.Point(179, 490);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = false;
            this.txtPassword.Size = new System.Drawing.Size(150, 22);
            this.txtPassword.TabIndex = 6;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Active1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnRegistrar.Active2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(168)))), ((int)(((byte)(183)))));
            this.btnRegistrar.BackColor = System.Drawing.Color.Transparent;
            this.btnRegistrar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRegistrar.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.btnRegistrar.ForeColor = System.Drawing.Color.White;
            this.btnRegistrar.Inactive1 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrar.Inactive2 = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrar.Location = new System.Drawing.Point(286, 618);
            this.btnRegistrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Radius = 10;
            this.btnRegistrar.Size = new System.Drawing.Size(106, 53);
            this.btnRegistrar.Stroke = false;
            this.btnRegistrar.StrokeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrar.TabIndex = 9;
            this.btnRegistrar.Text = "Guardar";
            this.btnRegistrar.Transparency = false;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // txtDomicilio
            // 
            this.txtDomicilio.BackColor = System.Drawing.Color.Transparent;
            this.txtDomicilio.Br = System.Drawing.Color.White;
            this.txtDomicilio.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtDomicilio.ForeColor = System.Drawing.Color.DimGray;
            this.txtDomicilio.Location = new System.Drawing.Point(176, 253);
            this.txtDomicilio.Margin = new System.Windows.Forms.Padding(2);
            this.txtDomicilio.Name = "txtDomicilio";
            this.txtDomicilio.PasswordChar = false;
            this.txtDomicilio.Size = new System.Drawing.Size(361, 22);
            this.txtDomicilio.TabIndex = 10;
            // 
            // cmbDepartamento
            // 
            this.cmbDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartamento.FormattingEnabled = true;
            this.cmbDepartamento.Location = new System.Drawing.Point(179, 320);
            this.cmbDepartamento.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDepartamento.Name = "cmbDepartamento";
            this.cmbDepartamento.Size = new System.Drawing.Size(90, 21);
            this.cmbDepartamento.TabIndex = 11;
            this.cmbDepartamento.SelectedIndexChanged += new System.EventHandler(this.cmbDepartamento_SelectedIndexChanged);
            this.cmbDepartamento.DropDownClosed += new System.EventHandler(this.cmbDepartamento_DropDownClosed);
            // 
            // cmbProvincia
            // 
            this.cmbProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProvincia.FormattingEnabled = true;
            this.cmbProvincia.Location = new System.Drawing.Point(309, 320);
            this.cmbProvincia.Margin = new System.Windows.Forms.Padding(2);
            this.cmbProvincia.Name = "cmbProvincia";
            this.cmbProvincia.Size = new System.Drawing.Size(83, 21);
            this.cmbProvincia.TabIndex = 12;
            this.cmbProvincia.SelectedIndexChanged += new System.EventHandler(this.cmbProvincia_SelectedIndexChanged);
            this.cmbProvincia.DropDownClosed += new System.EventHandler(this.cmbProvincia_DropDownClosed);
            // 
            // cmbDistrito
            // 
            this.cmbDistrito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrito.FormattingEnabled = true;
            this.cmbDistrito.Location = new System.Drawing.Point(420, 320);
            this.cmbDistrito.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDistrito.Name = "cmbDistrito";
            this.cmbDistrito.Size = new System.Drawing.Size(102, 21);
            this.cmbDistrito.TabIndex = 13;
            this.cmbDistrito.SelectedIndexChanged += new System.EventHandler(this.cmbDistrito_SelectedIndexChanged);
            // 
            // altoSlidingLabel1
            // 
            this.altoSlidingLabel1.Font = new System.Drawing.Font("Century Gothic", 17F, System.Drawing.FontStyle.Bold);
            this.altoSlidingLabel1.ForeColor = System.Drawing.Color.SeaGreen;
            this.altoSlidingLabel1.Location = new System.Drawing.Point(238, 56);
            this.altoSlidingLabel1.Margin = new System.Windows.Forms.Padding(2);
            this.altoSlidingLabel1.Name = "altoSlidingLabel1";
            this.altoSlidingLabel1.Size = new System.Drawing.Size(276, 25);
            this.altoSlidingLabel1.Slide = false;
            this.altoSlidingLabel1.TabIndex = 17;
            this.altoSlidingLabel1.Text = "REGISTRO COLABORADOR";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 37);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(146, 124);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(2, 27);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(678, 705);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // restaurar
            // 
            this.restaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.restaurar.Image = ((System.Drawing.Image)(resources.GetObject("restaurar.Image")));
            this.restaurar.Location = new System.Drawing.Point(621, 6);
            this.restaurar.Margin = new System.Windows.Forms.Padding(2);
            this.restaurar.Name = "restaurar";
            this.restaurar.Size = new System.Drawing.Size(19, 20);
            this.restaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.restaurar.TabIndex = 3;
            this.restaurar.TabStop = false;
            this.restaurar.Visible = false;
            this.restaurar.Click += new System.EventHandler(this.restaurar_Click_1);
            // 
            // minimizar
            // 
            this.minimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.minimizar.Image = ((System.Drawing.Image)(resources.GetObject("minimizar.Image")));
            this.minimizar.Location = new System.Drawing.Point(595, 2);
            this.minimizar.Margin = new System.Windows.Forms.Padding(2);
            this.minimizar.Name = "minimizar";
            this.minimizar.Size = new System.Drawing.Size(17, 28);
            this.minimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.minimizar.TabIndex = 2;
            this.minimizar.TabStop = false;
            this.minimizar.Click += new System.EventHandler(this.minimizar_Click_1);
            // 
            // cerrar
            // 
            this.cerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cerrar.Image = ((System.Drawing.Image)(resources.GetObject("cerrar.Image")));
            this.cerrar.Location = new System.Drawing.Point(652, 6);
            this.cerrar.Margin = new System.Windows.Forms.Padding(2);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(19, 20);
            this.cerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cerrar.TabIndex = 0;
            this.cerrar.TabStop = false;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click_1);
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BarraTitulo.Controls.Add(this.maximizar);
            this.BarraTitulo.Controls.Add(this.restaurar);
            this.BarraTitulo.Controls.Add(this.minimizar);
            this.BarraTitulo.Controls.Add(this.cerrar);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.BarraTitulo.Margin = new System.Windows.Forms.Padding(2);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(680, 23);
            this.BarraTitulo.TabIndex = 20;
            this.BarraTitulo.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            // 
            // maximizar
            // 
            this.maximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maximizar.Image = ((System.Drawing.Image)(resources.GetObject("maximizar.Image")));
            this.maximizar.Location = new System.Drawing.Point(621, 6);
            this.maximizar.Margin = new System.Windows.Forms.Padding(2);
            this.maximizar.Name = "maximizar";
            this.maximizar.Size = new System.Drawing.Size(19, 20);
            this.maximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.maximizar.TabIndex = 21;
            this.maximizar.TabStop = false;
            this.maximizar.Click += new System.EventHandler(this.maximizar_Click_1);
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtUsuario.Br = System.Drawing.Color.White;
            this.txtUsuario.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtUsuario.ForeColor = System.Drawing.Color.DimGray;
            this.txtUsuario.Location = new System.Drawing.Point(331, 434);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(2);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.PasswordChar = false;
            this.txtUsuario.Size = new System.Drawing.Size(222, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtDocumento
            // 
            this.txtDocumento.BackColor = System.Drawing.Color.Transparent;
            this.txtDocumento.Br = System.Drawing.Color.White;
            this.txtDocumento.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtDocumento.ForeColor = System.Drawing.Color.DimGray;
            this.txtDocumento.Location = new System.Drawing.Point(336, 374);
            this.txtDocumento.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.PasswordChar = false;
            this.txtDocumento.Size = new System.Drawing.Size(201, 22);
            this.txtDocumento.TabIndex = 22;
            // 
            // cmbTipoDocumento
            // 
            this.cmbTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoDocumento.FormattingEnabled = true;
            this.cmbTipoDocumento.Location = new System.Drawing.Point(179, 374);
            this.cmbTipoDocumento.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTipoDocumento.Name = "cmbTipoDocumento";
            this.cmbTipoDocumento.Size = new System.Drawing.Size(83, 21);
            this.cmbTipoDocumento.TabIndex = 23;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(179, 562);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaNacimiento.TabIndex = 25;
            this.dtpFechaNacimiento.Value = new System.DateTime(2021, 12, 9, 0, 0, 0, 0);
            // 
            // txtVerificarContra
            // 
            this.txtVerificarContra.BackColor = System.Drawing.Color.Transparent;
            this.txtVerificarContra.Br = System.Drawing.Color.White;
            this.txtVerificarContra.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.txtVerificarContra.ForeColor = System.Drawing.Color.DimGray;
            this.txtVerificarContra.Location = new System.Drawing.Point(347, 490);
            this.txtVerificarContra.Margin = new System.Windows.Forms.Padding(2);
            this.txtVerificarContra.Name = "txtVerificarContra";
            this.txtVerificarContra.PasswordChar = true;
            this.txtVerificarContra.Size = new System.Drawing.Size(175, 22);
            this.txtVerificarContra.TabIndex = 27;
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbNombre.Location = new System.Drawing.Point(176, 123);
            this.lbNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(70, 16);
            this.lbNombre.TabIndex = 28;
            this.lbNombre.Text = "Nombre *";
            // 
            // lbApellidoPaterno
            // 
            this.lbApellidoPaterno.AutoSize = true;
            this.lbApellidoPaterno.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbApellidoPaterno.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbApellidoPaterno.Location = new System.Drawing.Point(176, 175);
            this.lbApellidoPaterno.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbApellidoPaterno.Name = "lbApellidoPaterno";
            this.lbApellidoPaterno.Size = new System.Drawing.Size(131, 16);
            this.lbApellidoPaterno.TabIndex = 30;
            this.lbApellidoPaterno.Text = "Apellido Paterno  *";
            // 
            // lbApellidoMaterno
            // 
            this.lbApellidoMaterno.AutoSize = true;
            this.lbApellidoMaterno.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbApellidoMaterno.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbApellidoMaterno.Location = new System.Drawing.Point(357, 175);
            this.lbApellidoMaterno.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbApellidoMaterno.Name = "lbApellidoMaterno";
            this.lbApellidoMaterno.Size = new System.Drawing.Size(136, 16);
            this.lbApellidoMaterno.TabIndex = 31;
            this.lbApellidoMaterno.Text = "Apellido Materno  *";
            // 
            // lbDomicilio
            // 
            this.lbDomicilio.AutoSize = true;
            this.lbDomicilio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDomicilio.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbDomicilio.Location = new System.Drawing.Point(180, 235);
            this.lbDomicilio.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbDomicilio.Name = "lbDomicilio";
            this.lbDomicilio.Size = new System.Drawing.Size(83, 16);
            this.lbDomicilio.TabIndex = 32;
            this.lbDomicilio.Text = "Domicilio  *";
            // 
            // lbDepartamento
            // 
            this.lbDepartamento.AutoSize = true;
            this.lbDepartamento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDepartamento.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbDepartamento.Location = new System.Drawing.Point(176, 293);
            this.lbDepartamento.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbDepartamento.Name = "lbDepartamento";
            this.lbDepartamento.Size = new System.Drawing.Size(115, 16);
            this.lbDepartamento.TabIndex = 33;
            this.lbDepartamento.Text = "Departamento  *";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SeaGreen;
            this.label2.Location = new System.Drawing.Point(306, 293);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 34;
            this.label2.Text = "Provincia  *";
            // 
            // lbDistrito
            // 
            this.lbDistrito.AutoSize = true;
            this.lbDistrito.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDistrito.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbDistrito.Location = new System.Drawing.Point(417, 293);
            this.lbDistrito.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbDistrito.Name = "lbDistrito";
            this.lbDistrito.Size = new System.Drawing.Size(66, 16);
            this.lbDistrito.TabIndex = 35;
            this.lbDistrito.Text = "Distrito  *";
            // 
            // lbTipoDocumento
            // 
            this.lbTipoDocumento.AutoSize = true;
            this.lbTipoDocumento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoDocumento.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbTipoDocumento.Location = new System.Drawing.Point(176, 355);
            this.lbTipoDocumento.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTipoDocumento.Name = "lbTipoDocumento";
            this.lbTipoDocumento.Size = new System.Drawing.Size(126, 16);
            this.lbTipoDocumento.TabIndex = 36;
            this.lbTipoDocumento.Text = "Tipo Documento  *";
            // 
            // lbDocumento
            // 
            this.lbDocumento.AutoSize = true;
            this.lbDocumento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDocumento.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbDocumento.Location = new System.Drawing.Point(333, 355);
            this.lbDocumento.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbDocumento.Name = "lbDocumento";
            this.lbDocumento.Size = new System.Drawing.Size(95, 16);
            this.lbDocumento.TabIndex = 37;
            this.lbDocumento.Text = "Documento  *";
            // 
            // lbCelular
            // 
            this.lbCelular.AutoSize = true;
            this.lbCelular.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCelular.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbCelular.Location = new System.Drawing.Point(176, 416);
            this.lbCelular.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCelular.Name = "lbCelular";
            this.lbCelular.Size = new System.Drawing.Size(70, 16);
            this.lbCelular.TabIndex = 38;
            this.lbCelular.Text = "Celular  *";
            // 
            // lbCorreo
            // 
            this.lbCorreo.AutoSize = true;
            this.lbCorreo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCorreo.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbCorreo.Location = new System.Drawing.Point(333, 416);
            this.lbCorreo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCorreo.Name = "lbCorreo";
            this.lbCorreo.Size = new System.Drawing.Size(66, 16);
            this.lbCorreo.TabIndex = 39;
            this.lbCorreo.Text = "Correo  *";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SeaGreen;
            this.label1.Location = new System.Drawing.Point(176, 472);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 40;
            this.label1.Text = "Contraseña *";
            // 
            // lbVerificarContrasena
            // 
            this.lbVerificarContrasena.AutoSize = true;
            this.lbVerificarContrasena.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVerificarContrasena.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbVerificarContrasena.Location = new System.Drawing.Point(344, 472);
            this.lbVerificarContrasena.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbVerificarContrasena.Name = "lbVerificarContrasena";
            this.lbVerificarContrasena.Size = new System.Drawing.Size(158, 16);
            this.lbVerificarContrasena.TabIndex = 41;
            this.lbVerificarContrasena.Text = "Verrificar Contraseña *";
            // 
            // lbFechaNacimiento
            // 
            this.lbFechaNacimiento.AutoSize = true;
            this.lbFechaNacimiento.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaNacimiento.ForeColor = System.Drawing.Color.SeaGreen;
            this.lbFechaNacimiento.Location = new System.Drawing.Point(176, 534);
            this.lbFechaNacimiento.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbFechaNacimiento.Name = "lbFechaNacimiento";
            this.lbFechaNacimiento.Size = new System.Drawing.Size(136, 16);
            this.lbFechaNacimiento.TabIndex = 42;
            this.lbFechaNacimiento.Text = "Fecha Nacimiento *";
            // 
            // FrmRegistroColaborador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(680, 730);
            this.Controls.Add(this.lbFechaNacimiento);
            this.Controls.Add(this.lbVerificarContrasena);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCorreo);
            this.Controls.Add(this.lbCelular);
            this.Controls.Add(this.lbDocumento);
            this.Controls.Add(this.lbTipoDocumento);
            this.Controls.Add(this.lbDistrito);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbDepartamento);
            this.Controls.Add(this.lbDomicilio);
            this.Controls.Add(this.lbApellidoMaterno);
            this.Controls.Add(this.lbApellidoPaterno);
            this.Controls.Add(this.lbNombre);
            this.Controls.Add(this.txtVerificarContra);
            this.Controls.Add(this.dtpFechaNacimiento);
            this.Controls.Add(this.cmbTipoDocumento);
            this.Controls.Add(this.txtDocumento);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.BarraTitulo);
            this.Controls.Add(this.altoSlidingLabel1);
            this.Controls.Add(this.cmbDistrito);
            this.Controls.Add(this.cmbProvincia);
            this.Controls.Add(this.cmbDepartamento);
            this.Controls.Add(this.txtDomicilio);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.txtApellidoMaterno);
            this.Controls.Add(this.txtApellidoPaterno);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmRegistroColaborador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.FrmRegistro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.restaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cerrar)).EndInit();
            this.BarraTitulo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.maximizar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private AltoControls.AltoTextBox txtNombre;
        private AltoControls.AltoTextBox txtApellidoPaterno;
        private AltoControls.AltoTextBox txtApellidoMaterno;
        private AltoControls.AltoTextBox txtTelefono;
        private AltoControls.AltoTextBox txtPassword;
        private AltoControls.AltoButton btnRegistrar;
        private AltoControls.AltoTextBox txtDomicilio;
        private System.Windows.Forms.ComboBox cmbDepartamento;
        private System.Windows.Forms.ComboBox cmbProvincia;
        private System.Windows.Forms.ComboBox cmbDistrito;
        private AltoControls.AltoSlidingLabel altoSlidingLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox restaurar;
        private System.Windows.Forms.PictureBox minimizar;
        private System.Windows.Forms.PictureBox cerrar;
        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.PictureBox maximizar;
        private AltoControls.AltoTextBox txtUsuario;
        private AltoControls.AltoTextBox txtDocumento;
        private System.Windows.Forms.ComboBox cmbTipoDocumento;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private AltoControls.AltoTextBox txtVerificarContra;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Label lbApellidoPaterno;
        private System.Windows.Forms.Label lbApellidoMaterno;
        private System.Windows.Forms.Label lbDomicilio;
        private System.Windows.Forms.Label lbDepartamento;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbDistrito;
        private System.Windows.Forms.Label lbTipoDocumento;
        private System.Windows.Forms.Label lbDocumento;
        private System.Windows.Forms.Label lbCelular;
        private System.Windows.Forms.Label lbCorreo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbVerificarContrasena;
        private System.Windows.Forms.Label lbFechaNacimiento;
    }
}