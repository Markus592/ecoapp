﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace WindowsFormsApp1
{
    public partial class FrmUsuarios : Form
    {
        public FrmUsuarios()
        {
            InitializeComponent();
            DgvUsers.MultiSelect = false;

        }

        public FrmUsuarios(DataTable objTabla)
        {
            InitializeComponent();
            DgvUsers.DataSource = objTabla;
            DgvUsers.MultiSelect = false;
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {


        }



        private void txtDocumento_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdateUser_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

        }

       

        private void btnSearchUser_Click(object sender, EventArgs e)
        {
            DataTable tabla = new N_Entidad().AyudaEntidadXDoc(txtDocumento.Text);
            DgvUsers.MultiSelect = false;
            DgvUsers.DataSource = tabla;
        }

        private void FrmUsuarios_Load_1(object sender, EventArgs e)
        {

        }

        private void btnAddUser_Click_1(object sender, EventArgs e)
        {
            FrmRegistroColaborador registroColaborador = new FrmRegistroColaborador();
            registroColaborador.Show();
        }
    }
}
