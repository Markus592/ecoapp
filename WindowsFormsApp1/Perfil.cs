﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaEntidad;
namespace WindowsFormsApp1
{
    public partial class Perfil : Form
    {
        public Perfil()
        {
            InitializeComponent();
        }
        E_Entidad entidad = new E_Entidad();
        N_Entidad n_Entidad = new N_Entidad();
        public Perfil(int codigoEntidad)
        {            
            InitializeComponent();
            RecargarData(codigoEntidad);           
        }

        private void Perfil_Load(object sender, EventArgs e)
        {

        }

        private void btnActualizarPerfil_Click(object sender, EventArgs e)
        {
            try
            {
                
                entidad.Usuario = txtCorreo.Text;
                entidad.Nombres= txtNombre.Text;
                entidad.ApellidoPaterno= txtApPaterno.Text;
                entidad.ApellidoMaterno = txtApMaterno.Text;
                entidad.NumeroTelefonico = txtCelular.Text;
                if (n_Entidad.ActualizarPerfil(entidad))
                {   
                    MessageBox.Show("se actualizo de manera correcta el perfil");
                    RecargarData(entidad.Codigo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void  RecargarData(int codigo)
        {
            try
            {
                entidad = n_Entidad.MostrarPerfilEntidad(codigo);
                txtNombre.Text = entidad.Nombres;
                txtApPaterno.Text = entidad.ApellidoPaterno;
                txtApMaterno.Text = entidad.ApellidoMaterno;
                txtCelular.Text = entidad.NumeroTelefonico;
                txtCorreo.Text = entidad.Usuario;
                txtDomicilio.Text = entidad.Direccion;
                //if (entidad.RolAdmin||entidad.RolAdmin==null) { 

                //}
                //else
                //    {
                //    txtNombre.Enabled = false;
                //    txtApMaterno.Enabled = false;
                //    txtApPaterno.Enabled = false;
                //    txtDomicilio.Enabled = false;

                //    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
