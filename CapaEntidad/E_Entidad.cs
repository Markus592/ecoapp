﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class E_Entidad
    {
        public Int32 Codigo { get; set; }
        public Int16 IdDocumento { get; set; }
        public Int16 IdUbigeo { get; set; }
        public Int16 IdUbigeoCodigoPadre { get; set; }

        public string Direccion { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string NumeroTelefonico { get; set; }
        public string RutaImagen { get; set; }
        public string Documento { get; set; }
        //tipo entidad true es colaborador
        //tipo entidad false es cliente
        public string TipoEntidad { get; set; }
        public Int16 Puntos { get; set; }
        public Boolean RolAdmin { get; set; }
        public Boolean ENTI_SEXO { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public Boolean Activo { get; set; }
    }
}
