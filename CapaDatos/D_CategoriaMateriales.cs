﻿using CapaEntidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CapaDatos
{
    public class D_CategoriaMateriales
    {
        SqlCommand objComando = new SqlCommand();
        E_Entidad e_Entidad;
        public D_CategoriaMateriales() {
            e_Entidad = new E_Entidad();
        }

        public DataTable ListarMateriales() {
            try
            {
                objComando = new D_SqlClient().ConstruirComando();
                objComando.CommandText = "Select_CategoriaMateriales";
                objComando.Parameters.Clear();
                objComando.Connection.Open();
                //objComando.ExecuteNonQuery(); Insert. Delete, Update
                SqlDataReader reader;
                reader = objComando.ExecuteReader(); //Select
                DataTable tabla = new DataTable();
                tabla.Load(reader);
                objComando.Connection.Close();
                return tabla;
            }
            catch (Exception)
            {

                throw new Exception("NO se pudieron listar las categorias de materiales");
            }
            
        }
    }
}
